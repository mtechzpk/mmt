package com.example.alarmmanager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {

    private String format;
    private Calendar myCalendar;
    SimpleDateFormat sdf;
    TextView DateView, TimeView;
    Button DateButton, TimeButton, AlarmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myCalendar = Calendar.getInstance();

        DateButton = findViewById(R.id.DateButton);
        TimeButton = findViewById(R.id.TimeButton);
        AlarmButton = findViewById(R.id.SetAlarmButton);
        DateView = findViewById(R.id.dateview);
        TimeView = findViewById(R.id.timeview);
        DateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();

            }

        });

        TimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTime();
            }
        });
        AlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlarm(DateView.getText().toString(), TimeView.getText().toString());
            }
        });
    }


    private void getTime() {


        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                if (selectedHour == 0) {

                    selectedHour += 12;

                    format = "AM";
                } else if (selectedHour == 12) {

                    format = "PM";

                } else if (selectedHour > 12) {

                    selectedHour -= 12;

                    format = "PM";

                } else {

                    format = "AM";
                }
                TimeView.setText(selectedHour + "/" + selectedMinute + "/" + format);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    public void getDate() {

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy";
                sdf = new SimpleDateFormat(myFormat, Locale.US);
                DateView.setText(sdf.format(myCalendar.getTime()));

            }

        };
        new DatePickerDialog(MainActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();


    }

    private void setAlarm(String date, String time) {

        if (!date.isEmpty() && !time.isEmpty()) {

           String dateandtime = date+"/"+time;
            Log.d("DateAndTime", "setAlarm: " + dateandtime);
            String[] dateArray = dateandtime.split("/");
            String day = dateArray[0];
            String month = dateArray[1];
            String year = dateArray[2];
            String hours = dateArray[3];
            String minutes = dateArray[4];
            String AM_PM = dateArray[5];
            myCalendar.set(Calendar.YEAR, Integer.parseInt(year));
            myCalendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            myCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day) - 1);
            myCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
            myCalendar.set(Calendar.MINUTE, Integer.parseInt(minutes));
            if(AM_PM.equalsIgnoreCase("AM")){
                myCalendar.set(Calendar.AM_PM, Calendar.AM);

            }
            if(AM_PM.equalsIgnoreCase("PM")){
                myCalendar.set(Calendar.AM_PM, Calendar.PM);

            }
            Intent intent = new Intent(MainActivity.this,BackgroundBroadCast.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this,0,intent,0);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            long timeinmillis = myCalendar.getTimeInMillis();
            final int SDK_INT = Build.VERSION.SDK_INT;

            if (SDK_INT < Build.VERSION_CODES.KITKAT) {
                if (alarmManager != null) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, timeinmillis, pendingIntent);
                }
            }
            else if (SDK_INT < Build.VERSION_CODES.M) {
                if (alarmManager != null) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeinmillis, pendingIntent);
                }
            }
            else {

                if (alarmManager != null) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeinmillis, pendingIntent);


                }
            }
        }
      /*  String myFormat = "dd/MM/yyyy";
        sdf = new SimpleDateFormat(myFormat, Locale.US);
        Log.d("NEWDATE", "setAlarm: " +sdf.format(myCalendar.getTime()));;*/
        }



}
