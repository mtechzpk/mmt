package co.zamtech.mmt.server;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class ssignup extends AsyncTask<String, Void, String> {
    protected ssignup(){
    }

    @Override
    public String doInBackground(String... params) {
        String login_url = params[0];
        try {
            URL url=new URL(login_url);
            HttpURLConnection hurlconnection= (HttpURLConnection) url.openConnection();
            hurlconnection.setRequestMethod("POST");
            hurlconnection.setDoOutput(true);
            hurlconnection.setDoInput(true);
            OutputStream os=hurlconnection.getOutputStream();
            BufferedWriter bf=new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
            String data_send= URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(params[1],"UTF-8")+"&"+
                    URLEncoder.encode("phone","UTF-8")+"="+URLEncoder.encode(params[2],"UTF-8")+"&"+
                    URLEncoder.encode("address","UTF-8")+"="+URLEncoder.encode(params[3],"UTF-8")+"&"+
                    URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(params[4],"UTF-8")+"&"+
                    URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(params[5],"UTF-8");
            bf.write(data_send);
            bf.flush();
            bf.close();
            os.close();
            InputStream is=hurlconnection.getInputStream();
            BufferedReader br=new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }

            br.close();
            is.close();
            hurlconnection.disconnect();
            return sb.toString();
        } catch (IOException e) {
            return "Unable to connect to internet";
            //e.printStackTrace();
        }
    }
}
