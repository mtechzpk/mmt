package co.zamtech.mmt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import co.zamtech.mmt.Adapter.SectionsAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.sFetchSection;

public class SeeAllSection extends AppCompatActivity {
    String secid,secname;
    ImageView aheader_back;
    TextView aheader_title;
    GridView gridsections;
    Context context;
    Activity activity;
    private Loading loading;
    private ArrayList<String> id=new ArrayList<>();
    private ArrayList<String> type=new ArrayList<>();
    private ArrayList<String> title=new ArrayList<>();
    private ArrayList<String> video=new ArrayList<>();
    private ArrayList<String> image=new ArrayList<>();
    SectionsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeallsection);
        init();
        fetchdata();
        aheader_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void fetchdata(){
        @SuppressLint("StaticFieldLeak") final sFetchSection sfs=new sFetchSection() {
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if (s.equals("error") || s.equals("Unable to connect to internet")) {
                    Log.d("check", "onPostExecute: No internet");
                    //Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    loading.showError();
                }else{
                        try {
                            JSONArray ja = new JSONArray(s);
                            JSONObject jo;
                            id.clear();type.clear();title.clear();video.clear();image.clear();
                            jo=ja.getJSONObject(0);
                            if(jo.getString("response").equals("yes")){
                                for(int i=0;i<ja.length();i++){
                                    jo=ja.getJSONObject(i);
                                    id.add(jo.getString("id"));
                                    type.add(jo.getString("type"));
                                    title.add(jo.getString("title"));
                                    video.add(jo.getString("video"));
                                    image.add(jo.getString("image"));
                                }
                                if(id.size()==0){
                                    gridsections.setVisibility(View.GONE);

                                }else{
                                    adapter = new SectionsAdapter(id, type, title, video,image, context);
                                    gridsections.setAdapter(adapter);

                                }
                            }else{
                                gridsections.setVisibility(View.GONE);
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    loading.hideLoading();
                }
            }
        };
        sfs.execute(getString(R.string.baseURL)+"allsections.php",secid, SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
    }
    public void init(){
        secid= Objects.requireNonNull(getIntent().getExtras()).getString("secid");
        secname=getIntent().getExtras().getString("secname");
        aheader_back=findViewById(R.id.aheader_back);
        aheader_title=findViewById(R.id.aheader_title);
        aheader_title.setText(secname);
        gridsections=findViewById(R.id.gridsections);
        context=SeeAllSection.this;
        activity=SeeAllSection.this;
        loading=new Loading();
        loading.assignLoading(getWindow().getDecorView());
    }
}
