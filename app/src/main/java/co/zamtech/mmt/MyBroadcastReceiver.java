package co.zamtech.mmt;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import androidx.core.app.NotificationCompat;


import co.zamtech.utilities.Utilities;

public class MyBroadcastReceiver extends BroadcastReceiver {

    Intent intentt;

    MediaPlayer mediaPlayer;

    public static Ringtone ringtone;
    public static Vibrator vibrator;

    @Override
    public void onReceive(Context context, Intent intent) {




        vibrator = (Vibrator)context.getSystemService(context.VIBRATOR_SERVICE);
        vibrator.vibrate(20000);

        Utilities.saveString(context,"alarm_status","false");



        intent = new Intent(context, MainActivity.class);
        Utilities.saveString(context,"form","notification");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        String type = Utilities.getString(context,"type");
        String Object = Utilities.getString(context,"object");
        showNotification(context,"Precious Cargo Alert",type +" for "+Object+" is ringing",intent);

//        Notification notification = new Notification.Builder(context)
//                .setContentTitle("Alarm Notifiction")
//                .setContentText("Your Alarm in On")
//                .setSmallIcon(R.drawable.ic_baseline_alarm_on_24)
//                .build();
//
//        NotificationManager notificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
//        notification.flags|= Notification.FLAG_AUTO_CANCEL;
//        notificationManager.notify(0,notification);

        Uri noti = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(context,noti);
        ringtone.play();



    }

    public void showNotification(Context context, String title, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(body);



        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );


        mBuilder.setContentIntent(resultPendingIntent);
//        mBuilder.addAction(R.drawable.email_ic, "Cancel", resultPendingIntent);


        notificationManager.notify(notificationId, mBuilder.build());
    }

}
