package co.zamtech.mmt.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import co.zamtech.mmt.Adapter.DoctorAdapter;
import co.zamtech.mmt.Adapter.OpAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.server.sFetchDoctor;

public class Fragment_doctors extends Fragment {
    private Context context;
    private Activity activity;
    private Loading loading;
    private TextView oplabel,doclabel;
    private RecyclerView oprecycler,docrecycler;
    private ArrayList<String> op=new ArrayList<>();
    private ArrayList<String> name=new ArrayList<>();
    private ArrayList<String> image=new ArrayList<>();
    private ArrayList<String> op_type=new ArrayList<>();
    private OpAdapter adapter;
    private DoctorAdapter docadapter;
    public Fragment_doctors() {
        // Required empty public constructor
    }
    public static Fragment_doctors newInstance() {
        return new Fragment_doctors();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_doctors, container, false);
        init(v);
        fetchdata(v);
        return v;
    }
    public void fetchdata(View view){
        @SuppressLint("StaticFieldLeak") final sFetchDoctor sfd=new sFetchDoctor(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    Snackbar.make(view, "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    loading.hideLoading();
                }else{
                    try {
                        //getting whole json string
                        JSONObject jsonObj = new JSONObject(s);

//extracting data array from json string
                        JSONArray ja_op = jsonObj.getJSONArray("operations");
                        JSONObject json_op = ja_op.getJSONObject(0);
                        if(json_op.getString("response").equals("yes")){
                            op.clear();
                            for(int i=0;i<ja_op.length();i++){
                                json_op=ja_op.getJSONObject(i);
                                op.add(json_op.getString("opname"));
                            }
                            if(op.size()==0){
                                oplabel.setVisibility(View.GONE);
                                oprecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                oprecycler.setLayoutManager(horizontalLayoutManager);
                                adapter = new OpAdapter(op, getContext());
                                //adapter.setClickListener(this);
                                oprecycler.setAdapter(adapter);

                            }
                        }else{
                            oplabel.setVisibility(View.GONE);
                            oprecycler.setVisibility(View.GONE);
                        }
                        JSONArray ja_doc = jsonObj.getJSONArray("doctors");
                        JSONObject json_doc = ja_doc.getJSONObject(0);
                        if(json_doc.getString("response").equals("yes")){
                            name.clear();image.clear();op_type.clear();
                            for(int i=0;i<ja_doc.length();i++){
                                json_doc=ja_doc.getJSONObject(i);

                                name.add(json_doc.getString("name"));
                                image.add(json_doc.getString("image"));
                                op_type.add(json_doc.getString("op_type"));
                            }
                            if(name.size()==0){
                                doclabel.setVisibility(View.GONE);
                                docrecycler.setVisibility(View.GONE);

                            }else{
                                docrecycler.setLayoutManager(new LinearLayoutManager(context));
                                docadapter= new DoctorAdapter(name, image, op_type, context);
                                docrecycler.setAdapter(docadapter);

                            }
                        }else{
                            doclabel.setVisibility(View.GONE);
                            docrecycler.setVisibility(View.GONE);
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                        Snackbar.make(view, "Something went wrong.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    }
                    loading.hideLoading();
                }

            }
        };
        sfd.execute(getString(R.string.baseURL)+"fetchdoctorsoroperations.php","getdata", SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
    }
    public void init(View view){
        context=getContext();
        activity=getActivity();
        oplabel=view.findViewById(R.id.oplabel);
        doclabel=view.findViewById(R.id.doclabel);
        oprecycler=view.findViewById(R.id.oprecycler);
        docrecycler=view.findViewById(R.id.docrecycler);
        loading=new Loading();
        loading.assignLoading(view);
        if(SharedHelper.getKey(context,SharedHelper.lang).equals("1")){
            oplabel.setText("آپریشن چل رہے ہیں: ");
            doclabel.setText("ڈاکٹر: ");
        }

    }
}
