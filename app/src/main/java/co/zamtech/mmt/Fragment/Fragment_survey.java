package co.zamtech.mmt.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import co.zamtech.mmt.Adapter.SurveyAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.server.sFetchSurvey;

public class Fragment_survey extends Fragment {
    private Context context;
    private Activity activity;
    private Loading loading;
    private RecyclerView recycler;
    private ArrayList<String> id=new ArrayList<>();
    private ArrayList<String> question=new ArrayList<>();
    private ArrayList<String> level=new ArrayList<>();
    private SurveyAdapter adapter;
    public Fragment_survey() {
        // Required empty public constructor
    }
    public static Fragment_survey newInstance() {
        return new Fragment_survey();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_survey, container, false);
        init(v);
        fetchdata(v);
        return v;
    }
    public void fetchdata(View view){
        @SuppressLint("StaticFieldLeak") final sFetchSurvey sfs=new sFetchSurvey(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    Snackbar.make(view, "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    loading.hideLoading();
                }else{
                    try {
                        JSONArray ja_main = new JSONArray(s);
                        JSONObject jo_main;
                        id.clear();question.clear();level.clear();
                        jo_main=ja_main.getJSONObject(0);
                        if(jo_main.getString("response").equals("yes")){
                            for(int i=0;i<ja_main.length();i++){
                                jo_main=ja_main.getJSONObject(i);
                                id.add(jo_main.getString("id"));
                                question.add(jo_main.getString("question"));
                                level.add("0");
                            }
                            if(id.size()==0){
                                recycler.setVisibility(View.GONE);
                            }else{
                                recycler.setLayoutManager(new LinearLayoutManager(context));
                                adapter= new SurveyAdapter(id, question, level, context);
                                recycler.setAdapter(adapter);
                            }
                        }else{
                            recycler.setVisibility(View.GONE);
                        }

                    }catch (JSONException e){
                        e.printStackTrace();
                        Snackbar.make(view, "Something went wrong.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    }
                    loading.hideLoading();
                }

            }
        };
        sfs.execute(getString(R.string.baseURL)+"fetchsurvey.php","getdata", SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
    }
    public void init(View view){
        context=getContext();
        activity=getActivity();
        recycler=view.findViewById(R.id.recycler);
        loading=new Loading();
        loading.assignLoading(view);
    }
}
