package co.zamtech.mmt.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import co.zamtech.mmt.Adapter.HorRecycler;
import co.zamtech.mmt.Adapter.slideshowAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.server.sFetchHome;

public class Fragment_home extends Fragment {
    private Context context;
    private Activity activity;
    private ViewPager homepager;
    private Loading loading;
    private ArrayList<String> slider=new ArrayList<>();
    private int currentPage = 0;
    private static Timer timer;
    private final long DELAY_MS = 1500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 5000; // time in milliseconds between successive task executions.
    private static Handler handler;
    private static Runnable update;
    public static String TAG="checkzamar";
    private TextView newslabel;
    private RecyclerView newsrecycler;
    private TextView eventlabel;
    private RecyclerView eventrecycler;
    private TextView postlabel;
    private RecyclerView postrecycler;
    private TextView videolabel;
    private RecyclerView videorecycler;
    private TextView researchlabel;
    private RecyclerView researchrecycler;
    private ArrayList<String> newsid=new ArrayList<>();
    private ArrayList<String> newstype=new ArrayList<>();
    private ArrayList<String> newstitle=new ArrayList<>();
    private ArrayList<String> newsvideo=new ArrayList<>();
    private ArrayList<String> newsimage=new ArrayList<>();
    private ArrayList<String> eventid=new ArrayList<>();
    private ArrayList<String> eventtype=new ArrayList<>();
    private ArrayList<String> eventtitle=new ArrayList<>();
    private ArrayList<String> eventvideo=new ArrayList<>();
    private ArrayList<String> eventimage=new ArrayList<>();
    private ArrayList<String> postid=new ArrayList<>();
    private ArrayList<String> posttype=new ArrayList<>();
    private ArrayList<String> posttitle=new ArrayList<>();
    private ArrayList<String> postvideo=new ArrayList<>();
    private ArrayList<String> postimage=new ArrayList<>();
    private ArrayList<String> videoid=new ArrayList<>();
    private ArrayList<String> videotype=new ArrayList<>();
    private ArrayList<String> videotitle=new ArrayList<>();
    private ArrayList<String> videovideo=new ArrayList<>();
    private ArrayList<String> videoimage=new ArrayList<>();
    private ArrayList<String> researchid=new ArrayList<>();
    private ArrayList<String> researchtype=new ArrayList<>();
    private ArrayList<String> researchtitle=new ArrayList<>();
    private ArrayList<String> researchvideo=new ArrayList<>();
    private ArrayList<String> researchimage=new ArrayList<>();
    private HorRecycler newsadapter;
    private HorRecycler eventadapter;
    private HorRecycler postadapter;
    private HorRecycler videoadapter;
    private HorRecycler researchadapter;
    public Fragment_home() {
        // Required empty public constructor
    }
    public static Fragment_home newInstance() {
        return new Fragment_home();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(handler!=null) {
            timer.cancel();
            handler.removeCallbacksAndMessages(update);
        }
        fetchHome();
    }
    @Override
    public void onPause() {
        if(handler!=null){
            timer.cancel();
            handler.removeCallbacksAndMessages(update);
        }
        super.onPause();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        init(v);
        return v;
    }
    private void fetchHome(){
        @SuppressLint("StaticFieldLeak") final sFetchHome sfh=new sFetchHome(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    //Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    loading.showError();
                }else{
                    try {
                        //getting whole json string
                        JSONObject jsonObj = new JSONObject(s);
//extracting data array from json string
                        JSONArray ja_image = jsonObj.getJSONArray("slider");
                        JSONObject json_image = ja_image.getJSONObject(0);
                        if(json_image.getString("response").equals("yes")){
                            slider.clear();
                            for(int i=0;i<ja_image.length();i++){
                                json_image=ja_image.getJSONObject(i);
                                slider.add(json_image.getString("image"));
                            }
                            slideshowAdapter vAdp = new slideshowAdapter(context,slider);
                            homepager.setAdapter(vAdp);
                            handler = new Handler();
                            update = new Runnable() {
                                public void run() {
                                    if (currentPage == slider.size()) {
                                        currentPage = 0;
                                    }
                                    homepager.setCurrentItem(currentPage++, true);
                                }
                            };
                            timer = new Timer(); // This will create a new Thread
                            timer.schedule(new TimerTask() { // task to be scheduled

                                @Override
                                public void run() {
                                    handler.post(update);
                                }
                            }, DELAY_MS, PERIOD_MS);
                        }else{
                            homepager.setVisibility(View.GONE);
                        }
                        JSONArray ja_news = jsonObj.getJSONArray("news");
                        JSONObject json_news = ja_news.getJSONObject(0);
                        if(json_news.getString("response").equals("yes")){
                            newsid.clear();newstype.clear();newstitle.clear();newsvideo.clear();newsimage.clear();
                            for(int i=0;i<ja_news.length();i++){
                                json_news=ja_news.getJSONObject(i);
                                newsid.add(json_news.getString("id"));
                                newstype.add(json_news.getString("type"));
                                newstitle.add(json_news.getString("title"));
                                newsvideo.add(json_news.getString("video"));
                                newsimage.add(json_news.getString("image"));
                            }
                            if(newsid.size()==0){
                                newslabel.setVisibility(View.GONE);
                                newsrecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                newsrecycler.setLayoutManager(horizontalLayoutManager);
                                newsadapter = new HorRecycler(newsid,newstype,newstitle,newsvideo,newsimage, "1", getContext());
                                //adapter.setClickListener(this);
                                newsrecycler.setAdapter(newsadapter);

                            }
                        }else{
                            newslabel.setVisibility(View.GONE);
                            newsrecycler.setVisibility(View.GONE);
                        }
                        JSONArray ja_event = jsonObj.getJSONArray("event");
                        JSONObject json_event = ja_event.getJSONObject(0);
                        if(json_event.getString("response").equals("yes")){
                            eventid.clear();eventtype.clear();eventtitle.clear();eventvideo.clear();eventimage.clear();
                            for(int i=0;i<ja_event.length();i++){
                                json_event=ja_event.getJSONObject(i);
                                eventid.add(json_event.getString("id"));
                                eventtitle.add(json_event.getString("title"));
                                eventtype.add(json_event.getString("type"));
                                eventvideo.add(json_event.getString("video"));
                                eventimage.add(json_event.getString("image"));
                            }
                            if(eventid.size()==0){
                                eventlabel.setVisibility(View.GONE);
                                eventrecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                eventrecycler.setLayoutManager(horizontalLayoutManager);
                                eventadapter = new HorRecycler(eventid,eventtype,eventtitle,eventvideo,eventimage, "2", getContext());
                                //adapter.setClickListener(this);
                                eventrecycler.setAdapter(eventadapter);

                            }
                        }else{
                            eventlabel.setVisibility(View.GONE);
                            eventrecycler.setVisibility(View.GONE);
                        }
                        JSONArray ja_post = jsonObj.getJSONArray("post");
                        JSONObject json_post = ja_post.getJSONObject(0);
                        if(json_post.getString("response").equals("yes")){
                            postid.clear();posttype.clear();posttitle.clear();postvideo.clear();postimage.clear();
                            for(int i=0;i<ja_post.length();i++){
                                json_post=ja_post.getJSONObject(i);
                                postid.add(json_post.getString("id"));
                                posttype.add(json_post.getString("type"));
                                posttitle.add(json_post.getString("title"));
                                postvideo.add(json_post.getString("video"));
                                postimage.add(json_post.getString("image"));
                            }
                            if(postid.size()==0){
                                postlabel.setVisibility(View.GONE);
                                postrecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                postrecycler.setLayoutManager(horizontalLayoutManager);
                                postadapter = new HorRecycler(postid,posttype,posttitle,postvideo,postimage, "3", getContext());
                                //adapter.setClickListener(this);
                                postrecycler.setAdapter(postadapter);

                            }
                        }else{
                            postlabel.setVisibility(View.GONE);
                            postrecycler.setVisibility(View.GONE);
                        }
                        JSONArray ja_video = jsonObj.getJSONArray("video");
                        JSONObject json_video = ja_video.getJSONObject(0);
                        if(json_video.getString("response").equals("yes")){
                            videoid.clear();videotype.clear();videotitle.clear();videovideo.clear();videoimage.clear();
                            for(int i=0;i<ja_video.length();i++){
                                json_video=ja_video.getJSONObject(i);
                                videoid.add(json_video.getString("id"));
                                videotype.add(json_video.getString("type"));
                                videotitle.add(json_video.getString("title"));
                                videovideo.add(json_video.getString("video"));
                                videoimage.add(json_video.getString("image"));
                            }
                            if(videoid.size()==0){
                                videolabel.setVisibility(View.GONE);
                                videorecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                videorecycler.setLayoutManager(horizontalLayoutManager);
                                videoadapter = new HorRecycler(videoid,videotype,videotitle,videovideo,videoimage, "4", getContext());
                                //adapter.setClickListener(this);
                                videorecycler.setAdapter(videoadapter);

                            }
                        }else{
                            postlabel.setVisibility(View.GONE);
                            postrecycler.setVisibility(View.GONE);
                        }
                        JSONArray ja_research = jsonObj.getJSONArray("research");
                        JSONObject json_research = ja_research.getJSONObject(0);
                        if(json_research.getString("response").equals("yes")){
                            researchid.clear();researchtype.clear();researchtitle.clear();researchvideo.clear();researchimage.clear();
                            for(int i=0;i<ja_research.length();i++){
                                json_research=ja_research.getJSONObject(i);
                                researchid.add(json_research.getString("id"));
                                researchtype.add(json_research.getString("type"));
                                researchtitle.add(json_research.getString("title"));
                                researchvideo.add(json_research.getString("video"));
                                researchimage.add(json_research.getString("image"));
                            }
                            if(researchid.size()==0){
                                researchlabel.setVisibility(View.GONE);
                                researchrecycler.setVisibility(View.GONE);

                            }else{
                                LinearLayoutManager horizontalLayoutManager
                                        = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                researchrecycler.setLayoutManager(horizontalLayoutManager);
                                researchadapter = new HorRecycler(researchid,researchtype,researchtitle,researchvideo,researchimage, "5", getContext());
                                //adapter.setClickListener(this);
                                researchrecycler.setAdapter(researchadapter);

                            }
                        }else{
                            researchlabel.setVisibility(View.GONE);
                            researchrecycler.setVisibility(View.GONE);
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    loading.hideLoading();
                }
            }
        };
        sfh.execute(getString(R.string.baseURL)+"fetchsliderorsections.php","getdata", SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
        Log.i("checkzamar", "fetchHome: "+SharedHelper.getKey(context,SharedHelper.lang));
    }

    private void init(View view){
        context=getContext();
        activity=getActivity();
        loading=new Loading();
        loading.assignLoading(view);
        homepager= view.findViewById(R.id.homepager);
        newslabel= view.findViewById(R.id.newslabel);
        newsrecycler= view.findViewById(R.id.newsrecycler);
        eventrecycler= view.findViewById(R.id.eventrecycler);
        eventlabel= view.findViewById(R.id.eventlabel);
        postrecycler= view.findViewById(R.id.postrecycler);
        postlabel= view.findViewById(R.id.postlabel);
        videorecycler= view.findViewById(R.id.videorecycler);
        videolabel= view.findViewById(R.id.videolabel);
        researchrecycler= view.findViewById(R.id.researchrecycler);
        researchlabel= view.findViewById(R.id.researchlabel);
        if(SharedHelper.getKey(context,SharedHelper.lang).equals("1")) {
            newslabel.setText("خبریں");
            eventlabel.setText("تقریب");
            postlabel.setText("پوسٹ");
            videolabel.setText("ویڈیوز");
            researchlabel.setText("تحقیق");
        }
    }
}
