package co.zamtech.mmt.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.Stripe;

public class Fragment_donate extends Fragment {
    private Context context;
    private Activity activity;
    private TextView detail,purposelabel,donatelabel;
    private EditText name,email,phone,address,amount,purpose;
    private ExtendedFloatingActionButton sumbit;
    private ImageView jazz_image,stripe_image;
    public Fragment_donate() {
        // Required empty public constructor
    }
    public static Fragment_donate newInstance() {
        return new Fragment_donate();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_donate, container, false);
        init(v);
        stripe_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, Stripe.class);
                startActivity(intent);

            }
        });
        return v;
    }
    public void init(View view) {
        detail = view.findViewById(R.id.detail);
        purposelabel = view.findViewById(R.id.purposelabel);
        donatelabel = view.findViewById(R.id.donatelabel);
        name = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        address = view.findViewById(R.id.address);
        amount = view.findViewById(R.id.amount);
        purpose = view.findViewById(R.id.purpose);
        sumbit = view.findViewById(R.id.sumbit);
        stripe_image = view.findViewById(R.id.stripe_image);
        jazz_image = view.findViewById(R.id.jazz_image);
        context = getContext();
        activity = getActivity();
        if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
            detail.setText("براہ کرم فارم کو پُر کریں اور کسی بھی دستیاب طریقہ کے ذریعے ادائیگی کریں ");
            purposelabel.setText("مقصد ");
            donatelabel.setText("ہمیں عطیہ کریں ");
            name.setHint("نام ");
            email.setHint("ای میل ");
            phone.setHint("فون ");
            address.setHint("پتہ ");
            amount.setHint("رقم ");
            purpose.setHint("مقصد ");
            sumbit.setText("جمع کرائیں ");
        }
    }
}
