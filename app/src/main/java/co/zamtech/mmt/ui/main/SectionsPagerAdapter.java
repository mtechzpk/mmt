package co.zamtech.mmt.ui.main;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import co.zamtech.mmt.R;
import co.zamtech.mmt.Tab1;
import co.zamtech.mmt.Tab2;
import co.zamtech.mmt.Tab3;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("ssd", String.valueOf(position)+"-----------------ertyreyrtyrtyryrtyretyrey-----------------------");
        Fragment f = null;
        switch (position){
            case 0:
                f=  new Tab1();
                break;
            case 1:
                f=  new Tab2();
                break;
            case 2:
                f=  new Tab3();
                break;

        }
        return f;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 3;
    }
}