package co.zamtech.mmt.Helper;


import androidx.fragment.app.Fragment;

public interface NavigationManager {
    void showFragment(Fragment fragment, String value);
    void addFragment(Fragment fragment, String value);
}
