package co.zamtech.mmt.Helper;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import co.zamtech.mmt.MainActivity;
import co.zamtech.mmt.R;

public class FragmentNavigationManager implements NavigationManager {
    private static FragmentNavigationManager mInstance;
    public static FragmentManager mFragmentManager;

    public static FragmentNavigationManager getmInstance(MainActivity mainActivity){
        if(mInstance==null)
            mInstance=new FragmentNavigationManager();
        mInstance.configure(mainActivity);
        return mInstance;
    }

    private void configure(final MainActivity mainActivity){
        mFragmentManager= mainActivity.getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (mFragmentManager.getBackStackEntryCount() > 0) {
                    String fragmentTag = mFragmentManager.getBackStackEntryAt(mFragmentManager.getBackStackEntryCount() - 1).getName();
                    assert fragmentTag != null;
                    if(fragmentTag.equals("home")){
                        mainActivity.resetFooter();
                        mainActivity.setFooter(0);
                    }else if(fragmentTag.equals("doctors")){
                        mainActivity.resetFooter();
                        mainActivity.setFooter(1);
                    }else if(fragmentTag.equals("donate")){
                        mainActivity.resetFooter();
                        mainActivity.setFooter(2);
                    }else if(fragmentTag.equals("moneybox")){
                        mainActivity.resetFooter();
                        mainActivity.setFooter(3);
                    }else if(fragmentTag.equals("survey")){
                        mainActivity.resetFooter();
                        mainActivity.setFooter(4);
                    }else{
                        mainActivity.resetFooter();
                    }
                } else {
                    mainActivity.finish();
                }
            }
        });
    }

    @Override
    public void showFragment(Fragment fragment, String value) {

        FragmentManager fm=mFragmentManager;
        FragmentTransaction ft=fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left,
                R.anim.enter_from_left,R.anim.exit_to_right).replace(R.id.container,fragment);
        ft.addToBackStack(value);
        ft.commit();
        //fm.executePendingTransactions();

        //showFragment(FragmentContent.newInstance(title),false);
    }
    @Override
    public void addFragment(Fragment fragment, String value) {

        FragmentManager fm=mFragmentManager;
        FragmentTransaction ft=fm.beginTransaction().add(R.id.container,fragment);
        ft.addToBackStack(value);
        ft.commit();
        //fm.executePendingTransactions();

        //showFragment(FragmentContent.newInstance(title),false);
    }


    private void showFragment(FragmentContent fragmentContent, boolean b) {

    }
}

