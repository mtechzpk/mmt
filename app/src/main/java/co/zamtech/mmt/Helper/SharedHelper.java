package co.zamtech.mmt.Helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedHelper {
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static String spname="mmt";

    public static String islogin="islogin";
    public static String userid="userid";
    public static String name="name";
    public static String email="email";
    public static String address="address";
    public static String phone="phone";
    public static String password="password";
    public static String lang="lang";
    public static String fcmtoken="fcmtoken";
    public static String lat="lat";
    public static String lng="lng";


    @SuppressLint("ApplySharedPref")
    public static void putKey(Context context, String Key, String Value) {
        sharedPreferences = context.getSharedPreferences(spname, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.commit();

    }

    public static String getKey(Context contextGetKey, String Key) {
        sharedPreferences = contextGetKey.getSharedPreferences(spname, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Key, "");

    }



}
