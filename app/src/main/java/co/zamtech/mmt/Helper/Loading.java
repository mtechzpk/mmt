package co.zamtech.mmt.Helper;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import co.zamtech.mmt.R;

public class Loading {
    private ConstraintLayout loadinglayout;
    private ProgressBar loading;
    private TextView error;

    public void assignLoading(View view){
        loadinglayout=view.findViewById(R.id.loadinglayout);
        loading=view.findViewById(R.id.loading);
        error=view.findViewById(R.id.error);
    }

    public void showLoading(){
        loadinglayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);
    }
    public void hideLoading(){
        loadinglayout.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
    }
    public void showError(){
        loadinglayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
    }
}
