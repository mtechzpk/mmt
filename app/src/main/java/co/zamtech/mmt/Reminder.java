package co.zamtech.mmt;

import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.zamtech.mmt.MySqlite.RemindersModel;

public class Reminder {
    private int id; // pid
    private String name; // id value for food item
    private String startDate; // name of food
    private String color; // quantity of food item
    private String whichEye; // measurement of food item
    private int numberOfDays; // expiration date of food item
    private int howOften; //  date of food item added
    private int noOfDrops;
    private int dropFrequency;

    public Reminder() {
    }

    public Reminder(int id, String name, String startDate, String color, String whichEye,
                    int numberOfDays, int howOften, int noOfDrops, int dropFrequency) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.color = color;
        this.whichEye = whichEye;
        this.numberOfDays = numberOfDays;
        this.howOften = howOften;
        this.noOfDrops = noOfDrops;
        this.dropFrequency = dropFrequency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getWhichEye() {
        return whichEye;
    }

    public void setWhichEye(String whichEye) {
        this.whichEye = whichEye;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public int getHowOften() {
        return howOften;
    }

    public void setHowOften(int howOften) {
        this.howOften = howOften;
    }

    public int getNoOfDrops() {
        return noOfDrops;
    }

    public void setNoOfDrops(int noOfDrops) {
        this.noOfDrops = noOfDrops;
    }

    public int getDropFrequency() {
        return dropFrequency;
    }

    public void setDropFrequency(int dropFrequency) {
        this.dropFrequency = dropFrequency;
    }

    public ArrayList<Reminder> getReminders(Context context){
        RemindersModel remindersModel = new RemindersModel(context);
        ArrayList<Reminder> reminderArrayList = new ArrayList<>();

        Cursor mCursor = remindersModel.getAllReminders();
        Reminder reminder;
        while (!mCursor.isAfterLast()) {
            reminder = new Reminder(Integer.parseInt(mCursor.getString(0)),
                    mCursor.getString(1), mCursor.getString(2),
                    mCursor.getString(3), mCursor.getString(4),
                    Integer.parseInt(mCursor.getString(5)),
                    Integer.parseInt(mCursor.getString(6)),
                    Integer.parseInt(mCursor.getString(7)),
                    Integer.parseInt(mCursor.getString(8)));
            mCursor.moveToNext();
            reminderArrayList.add(reminder);
        }
        return reminderArrayList;
    }

    public Reminder getRemindersById(Context context, int id){
        RemindersModel remindersModel = new RemindersModel(context);
        Cursor mCursor = remindersModel.getRemindersById(String.valueOf(id));
        Reminder reminder;
        if (!mCursor.isAfterLast()) {
            reminder = new Reminder(
                    Integer.parseInt(mCursor.getString(0)),//id
                    mCursor.getString(1),//name
                    mCursor.getString(2),//startdate
                    mCursor.getString(3),//color
                    mCursor.getString(4),//whicheye
                    Integer.parseInt(mCursor.getString(5)),//numberofdays
                    Integer.parseInt(mCursor.getString(6)),//howoften
                    Integer.parseInt(mCursor.getString(7)),//noOfDrops
                    Integer.parseInt(mCursor.getString(8)));//dropfrequency
            return reminder;

        }
        return null;
    }

    public String updateReminder(String name, String startDate, String color, String whichEye,
                               int noOfDays, int howOften, int noOfDrops,
                               int dropFrequency, String id, Context context){
        RemindersModel remindersModel = new RemindersModel(context);
        int result = remindersModel.updateReminder(name, startDate, color, whichEye, noOfDays, howOften,
                noOfDrops, dropFrequency, id);
        if(result > 0){
            return "Successfully updated ... ";
        }
        else{
            return "Something went wrong ...";
        }
    }

    public void deleteReminder(Context context ,int id){
        RemindersModel remindersModel = new RemindersModel(context);
        remindersModel.deleteReminder(String.valueOf(id));
    }

    public void printReminderLogE(){
        Log.e("Reminder Detail", "------------------------------");
        Log.e("id ", String.valueOf(this.getId()));
        Log.e("name ", this.getName());
        Log.e("startdate ", this.getStartDate());
        Log.e("color ", this.getColor());
        Log.e("whicheye ", this.getWhichEye());
        Log.e("NumberOfDays ", String.valueOf(this.getNumberOfDays()));
        Log.e("HowOften ", String.valueOf(this.getHowOften()));
        Log.e("DropFrequency ", String.valueOf(this.getDropFrequency()));
    }

}
