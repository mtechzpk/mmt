package co.zamtech.mmt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.ssignup;

public class Signup extends AppCompatActivity {
    EditText signup_name,signup_email,signup_phone,signup_address,signup_password,signup_retypepassword;
    TextView dont_have_account_label,signup_signin,get_register_yourself;
    ExtendedFloatingActionButton signup_sumbit;
    Loading loading;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        init();
        loading=new Loading();
        loading.assignLoading(getWindow().getDecorView());
        dont_have_account_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Login.class);
                startActivity(intent);
            }
        });
        signup_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Login.class);
                startActivity(intent);
            }
        });
        signup_sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkdata();
            }
        });
    }
    public void checkdata() {
        if (validateName())
            return;
        if (!validEmail())
            return;
        if (!validatePassword())
            return;
        if (!conPssword())
            return;
        if (!validateNumber())
            return;
        signup();
    }
    public void signup(){
        @SuppressLint("StaticFieldLeak") final ssignup sss=new ssignup(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    loading.hideLoading();
                }else{
                    try {
                        JSONArray ja = new JSONArray(s);
                        JSONObject jo;
                        jo=ja.getJSONObject(0);
                        if(jo.getString("response").equals("yes")){
                            SharedHelper.putKey(context,SharedHelper.userid,jo.getString("userid"));
                            SharedHelper.putKey(context,SharedHelper.name,signup_name.getText().toString());
                            SharedHelper.putKey(context,SharedHelper.email,signup_email.getText().toString());
                            SharedHelper.putKey(context,SharedHelper.address,signup_address.getText().toString());
                            SharedHelper.putKey(context,SharedHelper.phone,signup_phone.getText().toString());
                            SharedHelper.putKey(context,SharedHelper.password,signup_password.getText().toString());
                            SharedHelper.putKey(context,SharedHelper.islogin,"yes");
                            Intent intent = new Intent(context, MainActivity.class);
                            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                        Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    }
                    loading.hideLoading();
                }

            }
        };
        sss.execute(getString(R.string.baseURL)+"signup.php",signup_name.getText().toString(),signup_phone.getText().toString(),signup_address.getText().toString(),signup_email.getText().toString(),signup_password.getText().toString());
        loading.showLoading();
    }
    private boolean validateName() {
        if (signup_name.getText().toString().trim().isEmpty() || signup_name.getText().toString().trim().equals(" ")) {
            Snackbar.make(getWindow().getDecorView(), "Your First Name", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_name.requestFocus();
            return true;
        } else if (signup_name.getText().length() < 3) {
            Snackbar.make(getWindow().getDecorView(), "Name must contain at least 3 characters", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_name.requestFocus();
            return true;
        } else if (signup_name.getText().length() >= 20) {
            Snackbar.make(getWindow().getDecorView(), "Name must contain at least 20 characters", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_name.requestFocus();
            return true;
        }
        return false;
    }
    private boolean validEmail() {
        if (signup_email.getText().toString().isEmpty()) {
            Snackbar.make(getWindow().getDecorView(), "Please enter Email", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_email.requestFocus();
            return false;
        } else if (!isValidEmail(signup_email.getText().toString())) {
            Snackbar.make(getWindow().getDecorView(), "Please enter Valid Email", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_email.requestFocus();
            return false;
        }
        return true;
    }
    private boolean isValidEmail(String m) {
        String emailPattern = "^[_A-Za-z0-9-+]+(\\.[A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Z-a-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher matcher = p.matcher(m);
        return matcher.matches();
    }
    private boolean validatePassword() {
        if (signup_password.getText().toString().isEmpty()) {
            Snackbar.make(getWindow().getDecorView(), "Enter password", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_password.requestFocus();
            return false;
        } else if (signup_password.getText().toString().length() < 6) {
            Snackbar.make(getWindow().getDecorView(), "Password min length 6", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_password.requestFocus();
            return false;
        } /*else if (!isValidPassword(signup_password.getText().toString())) {
            Snackbar.make(getWindow().getDecorView(), "Password must Contain small, capital & number", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_password.requestFocus();
            return false;
        }*/ else
            return true;
    }
    private boolean conPssword() {
        if (!signup_retypepassword.getText().toString().equals(signup_password.getText().toString())) {
            Snackbar.make(getWindow().getDecorView(), "Password not matched", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_retypepassword.requestFocus();
            return false;
        }
        return true;
    }
    private boolean validateNumber() {
        if (signup_phone.getText().toString().isEmpty() || signup_phone.getText().toString().equals(" ")) {
            Snackbar.make(getWindow().getDecorView(), "Enter Contact", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_phone.requestFocus();
            return false;
        } else if (signup_phone.getText().toString().length() < 9) {
            Snackbar.make(getWindow().getDecorView(), "Contact min length 9", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_phone.requestFocus();
            return false;
        } else if (signup_phone.getText().toString().length() > 15) {
            Snackbar.make(getWindow().getDecorView(), "Contact max length 15", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(this,R.color.snackbarcolor)).show();
            signup_phone.requestFocus();
            return false;
        } else
            return true;
    }
    public void init(){
        context=this;
        signup_name=findViewById(R.id.signup_name);
        signup_email=findViewById(R.id.signup_email);
        signup_phone=findViewById(R.id.signup_phone);
        signup_address=findViewById(R.id.signup_address);
        signup_password=findViewById(R.id.signup_password);
        signup_retypepassword=findViewById(R.id.signup_retypepassword);
        dont_have_account_label=findViewById(R.id.dont_have_account_label);
        signup_signin=findViewById(R.id.signup_signin);
        signup_sumbit=findViewById(R.id.signup_sumbit);
        get_register_yourself=findViewById(R.id.get_register_yourself);
        if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
            get_register_yourself.setText("سائن اپ ");
            signup_sumbit.setText("سائن اپ ");
            signup_signin.setText("پہلے سے ہی ایک اکاؤنٹ ہے ");
            dont_have_account_label.setText("سائن ان ");
            signup_signin.setTextColor(getResources().getColor(R.color.colorBlack));
            dont_have_account_label.setTextColor(getResources().getColor(R.color.colorBlue));
            signup_retypepassword.setHint("دوبارہ پاسوورڈ لکھئے ");
            signup_password.setHint("پاس ورڈ ");
            signup_address.setHint("پتہ ");
            signup_phone.setHint("فون ");
            signup_email.setHint("ای میل ");
            signup_name.setHint("نام ");
        }

    }
}
