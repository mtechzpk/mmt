package co.zamtech.mmt;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.prefs.Preferences;

import co.zamtech.mmt.MySqlite.AlarmsModel;
import co.zamtech.mmt.MySqlite.RemindersModel;
import co.zamtech.utilities.Utilities;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.PendingIntent.getActivity;
import static java.security.AccessController.getContext;

public class EditEyeDropReminderActivity extends AppCompatActivity {

    ImageView aheader_back;
    TextView aheader_title;

    private EditText nameEditText;
    private Spinner colorSpinner;
    private Spinner whichEyeSpinner;
    private TextView startDateTextView;
    private Spinner numberOfDaysSpinner;
    private Spinner howOftenSpinner;
    private Spinner tapperDrops1Spinner;
    private Spinner tapperDrops2Spinner;
    private Button updateButton;
    private Button deleteButton;
    private GridLayout gridLayout;
    private ArrayList<TextView> alarmsTextViewArray;
    private Calendar alarmToSetCalender;
    private int iNumberOfDay;
    private int ihowOften;
    private int[][] hourAndMinutes;
    private Reminder reminder;
    private Alarm alarm;
    private Calendar time;
    SimpleDateFormat sdf;
    ArrayAdapter<CharSequence> colorAdapter;
    ArrayAdapter<CharSequence> whichEyeAdapter;
    ArrayAdapter<CharSequence> numberOfDaysAdapter;
    int reminderId;
    int alarmLastId;
    AlarmsModel alarmsModel;
    Intent intent;
    AlarmManager alarmManager;
    CircleImageView profile_image;
    PendingIntent pendingIntent;
    boolean isReminderDisplayed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_eye_drop_reminder);
        isReminderDisplayed = false;
        reminderId = getIntent().getExtras().getInt("reminderID");
        alarmToSetCalender = Calendar.getInstance();

        intent = new Intent(EditEyeDropReminderActivity.this, AlarmReciever.class);
        alarmManager = (AlarmManager) EditEyeDropReminderActivity.this.getSystemService(EditEyeDropReminderActivity.ALARM_SERVICE);


        reminder = new Reminder();
        //alarm = new Alarm();
        aheader_back = findViewById(R.id.aheader_back);
        profile_image = findViewById(R.id.profile_image);
        aheader_title = findViewById(R.id.aheader_title);
        aheader_title.setText("Eye Drop Alarm");
        aheader_back.setOnClickListener(v -> {
            finish();
        });
//        String imageUri = Utilities.getString(EditEyeDropReminderActivity.this, "imageUri");
//        Picasso.get().load(imageUri).into(profile_image);
        alarmsTextViewArray = new ArrayList<>();
        nameEditText = (EditText) findViewById(R.id.reminder_name);
        colorSpinner = (Spinner) findViewById(R.id.color);
        startDateTextView = (TextView) findViewById(R.id.start_date);
        whichEyeSpinner = (Spinner) findViewById(R.id.which_eye);
        numberOfDaysSpinner = (Spinner) findViewById(R.id.number_of_days);
        howOftenSpinner = (Spinner) findViewById(R.id.how_often);
        gridLayout = (GridLayout) findViewById(R.id.grid_layout);
        time = Calendar.getInstance();

        alarmsModel = new AlarmsModel(EditEyeDropReminderActivity.this);
        alarmLastId = alarmsModel.getLastRecordId();

        sdf = new SimpleDateFormat("HH:mm");
        howOftenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clearAndAddAlarmsOnGrid(position);
                if (isReminderDisplayed) {

                } else {
                    showReminder(reminderId);
                }
            }//onitemselected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });//how often spinner listner

        intent = new Intent(EditEyeDropReminderActivity.this, AlarmReciever.class);
        //alarmManager = (AlarmManager)getContext().getSystemService(getContext().ALARM_SERVICE);

        tapperDrops1Spinner = (Spinner) findViewById(R.id.taper_drop1);//number of drop
        tapperDrops2Spinner = (Spinner) findViewById(R.id.taper_drop2);//frequency of drops
        updateButton = (Button) findViewById(R.id.update_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Alarm> alarmArrayList = alarm.getAlarmsByReminderId(EditEyeDropReminderActivity.this, reminderId);
                for (int i = 0; i < alarmArrayList.size(); i++) {
                    pendingIntent = PendingIntent.getBroadcast(EditEyeDropReminderActivity.this, alarmArrayList.get(i).getRequestCode(), intent, 0);
                    alarmManager.cancel(pendingIntent);
                    pendingIntent.cancel();
                }
                alarm.deleteAlarm(EditEyeDropReminderActivity.this, reminderId);

                String name = nameEditText.getText().toString();
                if (!name.equals("")) {
                    String color = colorSpinner.getSelectedItem().toString();
                    String startDate = startDateTextView.getText().toString();
                    String whichEye = whichEyeSpinner.getSelectedItem().toString();
                    String sNumberOfDay = numberOfDaysSpinner.getSelectedItem().toString();
                    if (sNumberOfDay.equals("OnGoing")) {
                        iNumberOfDay = -1;
                    } else {
                        iNumberOfDay = numberOfDaysSpinner.getSelectedItemPosition() + 1;
                    }

                    ihowOften = howOftenSpinner.getSelectedItemPosition() + 1;
                    int drop = tapperDrops1Spinner.getSelectedItemPosition();
                    int dropFrequency = tapperDrops2Spinner.getSelectedItemPosition() + 1;

                    hourAndMinutes = new int[ihowOften][2];

                    Reminder reminder = new Reminder();
                    //adding reminder to DB
                    reminder.updateReminder(name, startDate, color, whichEye, iNumberOfDay, ihowOften,
                            drop, dropFrequency, String.valueOf(reminderId), EditEyeDropReminderActivity.this);


                    nameEditText.setText("");
                    createAlarmsIntents();
                    Toast.makeText(EditEyeDropReminderActivity.this, "Reminder Successfully Updated", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(EditEyeDropReminderActivity.this, EyeDropAlarmTest.class);
                    startActivity(intent);


                } else {
                    Toast.makeText(EditEyeDropReminderActivity.this, "Please Enter Drop Name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        deleteButton = (Button) findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("id-", String.valueOf(reminderId));
                ArrayList<Alarm> alarmArrayList = alarm.getAlarmsByReminderId(EditEyeDropReminderActivity.this, reminderId);
                Log.e("size-", String.valueOf(alarmArrayList.size()));
                for (int i = 0; i < Tab1.alarmArrayList.size(); i++) {
//                    pendingIntent = PendingIntent.getBroadcast(EditEyeDropReminderActivity.this, alarmArrayList.get(i).getRequestCode(), intent,0);
//                    alarmManager.cancel(pendingIntent);
//                    pendingIntent.cancel();

                    for (int j = 0; j < alarmArrayList.size(); j++) {
                        if (Tab1.alarmArrayList.get(i).getId() == alarmArrayList.get(j).getId()) {
                            Tab1.alarmArrayList.remove(i);
                        }
                    }


                }

                for (int i = 0; i < Tab2.reminderArrayList.size(); i++) {
                    if (Tab2.reminderArrayList.get(i).getId() == reminderId) {
                        Tab2.reminderArrayList.remove(i);
                    }
                }


                alarm.deleteAlarm(EditEyeDropReminderActivity.this, reminderId);
                reminder.deleteReminder(EditEyeDropReminderActivity.this, reminderId);

                Toast.makeText(EditEyeDropReminderActivity.this, "Reminder Deleted Successfully !", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(EditEyeDropReminderActivity.this, EyeDropAlarmTest.class);
                startActivity(intent);
            }
        });

        colorAdapter = ArrayAdapter.createFromResource(this, R.array.colours, android.R.layout.simple_spinner_item);
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(colorAdapter);

        whichEyeAdapter = ArrayAdapter.createFromResource(this, R.array.which_eye, android.R.layout.simple_spinner_item);
        whichEyeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        whichEyeSpinner.setAdapter(whichEyeAdapter);


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        startDateTextView.setText(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
        DatePickerDialog ab = new DatePickerDialog(EditEyeDropReminderActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                alarmToSetCalender.set(Calendar.YEAR, year);
                alarmToSetCalender.set(Calendar.MONTH, month);
                alarmToSetCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDateTextView.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(month) + "/" + String.valueOf(year));

            }
        }, year, month, day);
        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.show();
            }
        });

        //showReminder(reminderId);
    }//on create

    public void showReminder(int id) {
        isReminderDisplayed = true;
        alarm = new Alarm();
        reminder = reminder.getRemindersById(EditEyeDropReminderActivity.this, id);
        nameEditText.setText(reminder.getName());
//        Picasso.get().load(reminder.getImage()).into(profile_image);
        startDateTextView.setText(reminder.getStartDate());
        colorSpinner.setSelection(colorAdapter.getPosition(reminder.getColor()));
        whichEyeSpinner.setSelection(whichEyeAdapter.getPosition(reminder.getWhichEye()));
        numberOfDaysSpinner.setSelection(reminder.getNumberOfDays() - 1);
        //howOftenSpinner.setSelection(reminder.getHowOften()-1);
        tapperDrops1Spinner.setSelection(reminder.getNoOfDrops());
        tapperDrops2Spinner.setSelection(reminder.getDropFrequency() - 1);

        //show alarm
        //Log.e("id",String.valueOf(id));
        //reminder.printReminderLogE();
        ArrayList<Alarm> alarmArrayList = new ArrayList<>();
        alarmArrayList = alarm.getAlarmsByReminderId(EditEyeDropReminderActivity.this, id);


        gridLayout.removeAllViews();
        alarmsTextViewArray.clear();
        for (int i = 0; i < alarmArrayList.size(); i++) {
            TextView alarm = new TextView(EditEyeDropReminderActivity.this);
            alarm.setTextSize(15);
            time.add(Calendar.HOUR, i);
            alarm.setText(alarmArrayList.get(i).getTime());
            alarm.setTextColor(Color.BLACK);


            alarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(EditEyeDropReminderActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                            if (selectedHour > 12) {

                                //alarm.setText(String.valueOf(selectedHour-12)+ ":"+(String.valueOf(minute)+"pm"));
                                time.set(Calendar.HOUR_OF_DAY, selectedHour - 12);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                            }
                            if (selectedHour == 12) {
                                //alarm.setText("12"+ ":"+(String.valueOf(minute)+"pm"));
                                time.set(Calendar.HOUR_OF_DAY, 12);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                            }
                            if (selectedHour < 12) {
                                //alarm.setText(String.valueOf(selectedHour)+ ":"+(String.valueOf(minute)+"am"));
                                time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " AM >  ");
                            }

                                    /*time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                    time.set(Calendar.MINUTE, selectedMinute);
                                    alarm.setText(sdf.format(time.getTime()) +  " >  ");*/


                        }
                    }, hour, minute, false);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            gridLayout.addView(alarm);
            alarmsTextViewArray.add(alarm);
        }

    }

    private void createAlarmsIntents() {

        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        AlarmsModel alarmsModel = new AlarmsModel(EditEyeDropReminderActivity.this);
        int requestCode = 0;//alarmsModel.getLastRequestCode();

        int hours, minutes;
        String AM_PM;
        for (int i = 0; i < iNumberOfDay; i++) {
            //Log.e(String.valueOf(i),"i");
            alarmToSetCalender.add(Calendar.DATE, i * ihowOften);
            for (int j = 0; j < alarmsTextViewArray.size(); j++) {
                //Log.e(String.valueOf(j),"j");
                ++requestCode;
                hours = Integer.parseInt((String) alarmsTextViewArray.get(j).getText().subSequence(0, 2));
                minutes = Integer.parseInt((String) alarmsTextViewArray.get(j).getText().subSequence(3, 5));
                AM_PM = (String) alarmsTextViewArray.get(j).getText().subSequence(6, 8);
                alarmToSetCalender.set(Calendar.HOUR_OF_DAY, hours);
                alarmToSetCalender.set(Calendar.MINUTE, minutes);

                if (AM_PM.equals("AM")) {
                    alarmToSetCalender.set(Calendar.AM_PM, Calendar.AM);
                } else {
                    alarmToSetCalender.set(Calendar.AM_PM, Calendar.PM);
                }

                alarmsModel.insertAlarms(alarmsTextViewArray.get(j).getText().toString(), requestCode,
                        String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                                String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                                String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId);

                alarmLastId++;
                if (Tab1.alarmArrayList != null) {
                    Tab1.addAlarmToArrayList(new Alarm(alarmLastId, alarmsTextViewArray.get(j).getText().toString(), requestCode, true, String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId));
                    //Toast.makeText(getContext() ,"not null",Toast.LENGTH_SHORT).show();
                } else {
                    Tab1.alarmArrayList = new ArrayList<>();
                    Tab1.addAlarmToArrayList(new Alarm(alarmLastId, alarmsTextViewArray.get(j).getText().toString(), requestCode, true, String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId));
                    //Toast.makeText(getContext() ,"null",Toast.LENGTH_SHORT).show();
                }

                Intent intent = new Intent(EditEyeDropReminderActivity.this, AlarmReciever.class);
                intent.putExtra("reminderId", reminderId);
                // Loop counter `i` is used as a `requestCode`
                PendingIntent pendingIntent = PendingIntent.getBroadcast(EditEyeDropReminderActivity.this, requestCode, intent, 0);
                // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)

                alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + (
                                alarmToSetCalender.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()), pendingIntent);
                //Toast.makeText(getContext() ,String.valueOf((alarmToSetCalender.getTimeInMillis()/1000)/60),Toast.LENGTH_SHORT).show();
                intentArray.add(pendingIntent);
                //Log.e("Alarm -----------------",String.valueOf((alarmToSetCalender.getTimeInMillis()/1000)/60));


            }
        }

    }

    public void clearAndAddAlarmsOnGrid(int count) {
        gridLayout.removeAllViews();
        alarmsTextViewArray.clear();
        for (int i = 0; i <= count; i++) {
            TextView alarm = new TextView(EditEyeDropReminderActivity.this);
            alarm.setTextSize(15);
            time.add(Calendar.HOUR, i);
//                    alarm.setText(time.get(Calendar.HOUR) +":"+ time.get(Calendar.MINUTE)   + "> ");
            alarm.setText("08:00 AM >  ");//sdf.format(time.getTime()) + " >  "
            alarm.setTextColor(Color.BLACK);


            alarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(EditEyeDropReminderActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                            if (selectedHour > 12) {

                                //alarm.setText(String.valueOf(selectedHour-12)+ ":"+(String.valueOf(minute)+"pm"));
                                time.set(Calendar.HOUR_OF_DAY, selectedHour - 12);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                            }
                            if (selectedHour == 12) {
                                //alarm.setText("12"+ ":"+(String.valueOf(minute)+"pm"));
                                time.set(Calendar.HOUR_OF_DAY, 12);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                            }
                            if (selectedHour < 12) {
                                //alarm.setText(String.valueOf(selectedHour)+ ":"+(String.valueOf(minute)+"am"));
                                time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                time.set(Calendar.MINUTE, selectedMinute);
                                alarm.setText(sdf.format(time.getTime()) + " AM >  ");
                            }

                                    /*time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                    time.set(Calendar.MINUTE, selectedMinute);
                                    alarm.setText(sdf.format(time.getTime()) +  " >  ");*/


                        }
                    }, hour, minute, false);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            gridLayout.addView(alarm);
            alarmsTextViewArray.add(alarm);
        }
    }


}