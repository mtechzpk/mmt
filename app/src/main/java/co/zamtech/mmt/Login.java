package co.zamtech.mmt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.slogin;

public class Login extends AppCompatActivity {
    EditText login_email,login_password;
    TextView login_forgot,dont_have_account_label,login_signup,login_label;
    ExtendedFloatingActionButton login_btn;
    Loading loading;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        loading=new Loading();
        loading.assignLoading(getWindow().getDecorView());
        login_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ForgotPassword.class);
                startActivity(intent);
            }
        });
        dont_have_account_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Signup.class);
                startActivity(intent);
            }
        });
        login_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Signup.class);
                startActivity(intent);
            }
        });
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }
    public void login(){
        if(!login_email.getText().toString().isEmpty()  && !login_email.getText().toString().equals("") && !login_email.getText().toString().equals(" ") && !login_password.getText().toString().isEmpty() && !login_password.getText().toString().equals("") && !login_password.getText().toString().equals(" ")){
            @SuppressLint("StaticFieldLeak") final slogin sli=new slogin(){
                @Override
                protected void onPostExecute(String s) {
                    Log.i("checkzamar", s);
                    if(s.equals("error") || s.equals("Unable to connect to internet")){
                        Log.d("check", "onPostExecute: No internet");
                        Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                        loading.hideLoading();
                    }else{
                        try {
                            JSONArray ja = new JSONArray(s);
                            JSONObject jo;
                            jo=ja.getJSONObject(0);
                            if(jo.getString("response").equals("yes")){
                                SharedHelper.putKey(context,SharedHelper.userid,jo.getString("userid"));
                                SharedHelper.putKey(context,SharedHelper.name,jo.getString("name"));
                                SharedHelper.putKey(context,SharedHelper.email,jo.getString("email"));
                                SharedHelper.putKey(context,SharedHelper.address,jo.getString("address"));
                                SharedHelper.putKey(context,SharedHelper.phone,jo.getString("phone"));
                                SharedHelper.putKey(context,SharedHelper.password,jo.getString("password"));
                                SharedHelper.putKey(context,SharedHelper.islogin,"yes");
                                Intent intent = new Intent(context, MainActivity.class);
                                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }else{
                                Snackbar.make(getWindow().getDecorView(), "Email or password incorrect.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                            Snackbar.make(getWindow().getDecorView(), "Email or password incorrect.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                        }
                        loading.hideLoading();
                    }

                }
            };
            sli.execute(getString(R.string.baseURL)+"signin.php",login_email.getText().toString(),login_password.getText().toString());
            loading.showLoading();
        }else{
            Snackbar.make(getWindow().getDecorView(), "Email and password cannot be empty.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
        }
    }
    public void init(){
        context=this;
        login_email=findViewById(R.id.login_email);
        login_password=findViewById(R.id.login_password);
        login_forgot=findViewById(R.id.login_forgot);
        dont_have_account_label=findViewById(R.id.dont_have_account_label);
        login_signup=findViewById(R.id.login_signup);
        login_btn=findViewById(R.id.login_btn);
        login_label=findViewById(R.id.login_label);
        if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
            login_label.setText("سائن ان ");
            login_btn.setText("لاگ ان کریں ");
            login_signup.setText("کیا آپ کا اکاؤنٹ نہیں ہے؟ ");
            dont_have_account_label.setText("سائن اپ ");
            login_signup.setTextColor(getResources().getColor(R.color.colorBlack));
            dont_have_account_label.setTextColor(getResources().getColor(R.color.colorBlue));
            login_forgot.setText("اپنا پاس ورڈ بھول گئے ");
            login_password.setHint("پاس ورڈ ");
            login_email.setHint("ای میل ");
        }
    }
}
