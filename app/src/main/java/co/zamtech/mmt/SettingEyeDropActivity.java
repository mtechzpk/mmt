package co.zamtech.mmt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.C;

import java.util.Set;

public class SettingEyeDropActivity extends AppCompatActivity {

    private ImageView aheader_back;
    private TextView aheader_title;
    private View layout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_eye_drop);
        layout1=findViewById(R.id.layout1);

        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingEyeDropActivity.this, SettingHowToUseEyeDropActivity.class);
                startActivity(intent);
            }
        });

        aheader_back=findViewById(R.id.aheader_back);
        aheader_back.setOnClickListener(v -> {
            finish();
        });
    }
}