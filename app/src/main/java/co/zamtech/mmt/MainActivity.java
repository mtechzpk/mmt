package co.zamtech.mmt;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.stripe.android.PaymentConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.zamtech.mmt.Adapter.SurveyAdapter;
import co.zamtech.mmt.Fragment.Fragment_doctors;
import co.zamtech.mmt.Fragment.Fragment_donate;
import co.zamtech.mmt.Fragment.Fragment_home;
import co.zamtech.mmt.Fragment.Fragment_moneybox;
import co.zamtech.mmt.Fragment.Fragment_survey;
import co.zamtech.mmt.Helper.FragmentNavigationManager;
import co.zamtech.mmt.Helper.NavigationManager;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.sFetchSurvey;
import co.zamtech.mmt.server.sSendFcm;
import co.zamtech.utilities.Utilities;

public class MainActivity extends AppCompatActivity implements LocationListener {
    public static NavigationManager navigationManager;
    ImageView footer_home, footer_doctors, footer_donate, footer_moneybox, footer_survey;
    TextView footer_home_label, footer_doctors_label, footer_donate_label, footer_moneybox_label, footer_survey_label;
    TextView header_dots;
    TextView header_title;
    Context context;
    String from="df";
    public static boolean showpopup = true;
    ToggleButton lang;
    protected LocationManager locationManager;
    protected LocationListener locationListener;

    @Override
    protected void onResume() {
        super.onResume();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //showpopup("zamar");
                //handler.postDelayed(this, 2000);
            }
        }, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
         from = Utilities.getString(MainActivity.this,"from");
        if (from.equals("notification")){

            if (MyBroadcastReceiver.ringtone != null){

                MyBroadcastReceiver.ringtone.stop();
                MyBroadcastReceiver.vibrator.cancel();


            }
//            if (ringtone != null) ringtone.stop();

//            Ringtone ringtone = RingtoneManager.getRingtone(UserInfoActivity.this,noti);
//            ringtone.stop();

        }
        if (SharedHelper.getKey(MainActivity.this, SharedHelper.lang).equals("")) {
            SharedHelper.putKey(MainActivity.this, SharedHelper.lang, "0");
            lang.setChecked(false);
        }
        if (SharedHelper.getKey(MainActivity.this, SharedHelper.lang).equals("0")) {
            lang.setChecked(false);
        } else {
            lang.setChecked(true);
        }
        if (savedInstanceState == null)
            selectFirstItemAsDefault();
        footerfunctions();
        header_dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initdots();
            }
        });
        lang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedHelper.putKey(context, SharedHelper.lang, "1");
                    Intent intent = new Intent(context, MainActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    SharedHelper.putKey(context, SharedHelper.lang, "0");
                    Intent intent = new Intent(context, MainActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });

        if (SharedHelper.getKey(context, SharedHelper.fcmtoken).equals("")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    SharedHelper.putKey(context, SharedHelper.fcmtoken, newToken);
                    sendtoken();
                }
            });
        }else{
            sendtoken();
        }

    }

    private void sendtoken() {
        @SuppressLint("StaticFieldLeak") final sSendFcm ssf = new sSendFcm() {
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamarfcm", s);
                if (s.equals("error") || s.equals("Unable to connect to internet")) {
                    Log.d("check", "onPostExecute: No internet");
                }else{
                    Log.i("checkzamar", "token sent: "+SharedHelper.getKey(context,SharedHelper.fcmtoken));
                }

            }
        };
        ssf.execute(getString(R.string.baseURL) + "notifications.php", SharedHelper.getKey(context, SharedHelper.fcmtoken));
        Log.i("checkzamarfcm", SharedHelper.getKey(context, SharedHelper.fcmtoken));
    }
    public void showpopupweather() {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_weather, null);
        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        ProgressBar webviewloading=popupView.findViewById(R.id.webviewloading);
        WebView browser =  popupView.findViewById(R.id.webview);
        browser.setWebViewClient(new AppWebViewClients(webviewloading));
        browser.getSettings().setLoadsImagesAutomatically(true);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        browser.loadUrl("http://api.openweathermap.org/data/2.5/weather?lat="+SharedHelper.getKey(context,SharedHelper.lat)+"&lon="+SharedHelper.getKey(context,SharedHelper.lng)+"&APPID=f473334deab91e25ec3b31a9dfabaa60&mode=html");
        Log.i("checkzamar", "showpopupweather: "+"http://api.openweathermap.org/data/2.5/weather?lat="+SharedHelper.getKey(context,SharedHelper.lat)+"&lon="+SharedHelper.getKey(context,SharedHelper.lng));
        ExtendedFloatingActionButton button = popupView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

    }
    public void showpopup(String msg) {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_home, null);
        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        TextView title = popupView.findViewById(R.id.title);
        ImageView image = popupView.findViewById(R.id.image);
        TextView detail = popupView.findViewById(R.id.detail);
        detail.setText(msg);
        ExtendedFloatingActionButton button = popupView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

    }

    private void selectFirstItemAsDefault() {
        if (navigationManager != null) {
            navigationManager.addFragment(Fragment_home.newInstance(), "home");
            setFooter(0);
        }
    }

    public void footerfunctions() {
        footer_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(0);
                navigationManager.showFragment(Fragment_home.newInstance(), "home");

            }
        });
        footer_doctors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(1);
                navigationManager.showFragment(Fragment_doctors.newInstance(), "doctors");

            }
        });
        footer_donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, Stripe.class);
                startActivity(intent);
                /*resetFooter();
                setFooter(2);
                navigationManager.showFragment(Fragment_donate.newInstance(), "donate");*/
            }
        });
        footer_moneybox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(3);
                navigationManager.showFragment(Fragment_moneybox.newInstance(), "moneybox");

            }
        });
        footer_survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(4);
                navigationManager.showFragment(Fragment_survey.newInstance(), "survey");
            }
        });
        footer_home_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(0);
                navigationManager.showFragment(Fragment_home.newInstance(), "home");

            }
        });
        footer_doctors_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetFooter();
                setFooter(1);
                navigationManager.showFragment(Fragment_doctors.newInstance(), "doctors");

            }
        });
        footer_donate_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, Stripe.class);
                startActivity(intent);
                /*resetFooter();
                setFooter(2);
                navigationManager.showFragment(Fragment_donate.newInstance(), "donate");*/
            }
        });
        footer_moneybox_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(3);
                navigationManager.showFragment(Fragment_moneybox.newInstance(), "moneybox");

            }
        });
        footer_survey_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFooter();
                setFooter(4);
                navigationManager.showFragment(Fragment_survey.newInstance(), "survey");
            }
        });
    }

    public void initdots() {
        final PopupMenu popup = new PopupMenu(context, header_dots);
        if (SharedHelper.getKey(context, SharedHelper.islogin).equals("yes")) {
            if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
                popup.inflate(R.menu.dots_menu_urdu);
            } else {
                popup.inflate(R.menu.dots_menu);
            }

        } else {
            if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
                popup.inflate(R.menu.dots_menu_login_urdu);
            } else {
                popup.inflate(R.menu.dots_menu_login);
            }
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.about:
                        Intent intenta = new Intent(context,About.class);
                        startActivity(intenta);
                        popup.dismiss();
                        return true;
                    case R.id.eye_drop_alarm://mianhamza
                        Intent intentEyeDropAlarm = new Intent(context,EyeDropAlarmTest.class);
                        startActivity(intentEyeDropAlarm);
                        popup.dismiss();
                        return true;
                    case R.id.contact:
                        Intent intentcontact = new Intent(context, ContactUs.class);
                        startActivity(intentcontact);
                        popup.dismiss();
                        return true;
                    case R.id.weather:
                        checkper(1);
                        /*if ( ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                            Toast.makeText(context,"Permissions are required",Toast.LENGTH_SHORT).show();
                        }else{
                            showpopupweather();
                        }*/
                        popup.dismiss();
                        return true;
                    case R.id.share:
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                "Hey check the official app of Muhammadi Medical Trust at: https://play.google.com/store/apps/details?id=co.zamtech.mmt");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        popup.dismiss();
                        return true;
                    case R.id.profile:
                        Intent intentz = new Intent(context,Profile.class);
                        startActivity(intentz);
                        popup.dismiss();
                        return true;
                    case R.id.logout:
                        SharedHelper.putKey(context, SharedHelper.userid, "");
                        SharedHelper.putKey(context, SharedHelper.name, "");
                        SharedHelper.putKey(context, SharedHelper.email, "");
                        SharedHelper.putKey(context, SharedHelper.address, "");
                        SharedHelper.putKey(context, SharedHelper.phone, "");
                        SharedHelper.putKey(context, SharedHelper.password, "");
                        SharedHelper.putKey(context, SharedHelper.islogin, "no");
                        Intent intent = new Intent(context, MainActivity.class);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        popup.dismiss();
                        return true;
                    case R.id.login:
                        Intent loginintent = new Intent(context, Login.class);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(loginintent);
                        popup.dismiss();
                        return true;
                    case R.id.privacy:
                        Intent intentpri = new Intent(context,Privacy.class);
                        startActivity(intentpri);
                        popup.dismiss();
                        return true;
                    default:
                        return true;
                }
            }
        });
        popup.show();
    }

    public void setFooter(int i) {
        if (i == 0) {
            header_title.setText("Muhammadi Medical Trust (Eye Hospital)");
            footer_home.setImageResource(R.drawable.homesel);
            footer_home_label.setTextColor(getResources().getColor(R.color.colorBlue));
        } else if (i == 1) {
            header_title.setText("Doctors");
            footer_doctors.setImageResource(R.drawable.doctorsel);
            footer_doctors_label.setTextColor(getResources().getColor(R.color.colorBlue));
        } else if (i == 2) {
            header_title.setText("Donate");
            footer_donate.setImageResource(R.drawable.donatesel);
            footer_donate_label.setTextColor(getResources().getColor(R.color.colorBlue));
        } else if (i == 3) {
            header_title.setText("Money Box");
            footer_moneybox.setImageResource(R.drawable.moneyboxsel);
            footer_moneybox_label.setTextColor(getResources().getColor(R.color.colorBlue));
        } else if (i == 4) {
            header_title.setText("Survey");
            footer_survey.setImageResource(R.drawable.surveysel);
            footer_survey_label.setTextColor(getResources().getColor(R.color.colorBlue));
        }
    }

    public void resetFooter() {
        footer_home.setImageResource(R.drawable.home);
        footer_doctors.setImageResource(R.drawable.doctor);
        footer_donate.setImageResource(R.drawable.donate);
        footer_moneybox.setImageResource(R.drawable.moneybox);
        footer_survey.setImageResource(R.drawable.survey);
        footer_home_label.setTextColor(getResources().getColor(R.color.greydark));
        footer_doctors_label.setTextColor(getResources().getColor(R.color.greydark));
        footer_donate_label.setTextColor(getResources().getColor(R.color.greydark));
        footer_moneybox_label.setTextColor(getResources().getColor(R.color.greydark));
        footer_survey_label.setTextColor(getResources().getColor(R.color.greydark));
    }

    public void init() {
        context = this;
        navigationManager = FragmentNavigationManager.getmInstance(MainActivity.this);
        footer_home = findViewById(R.id.footer_home);
        footer_doctors = findViewById(R.id.footer_doctors);
        footer_donate = findViewById(R.id.footer_donate);
        footer_moneybox = findViewById(R.id.footer_moneybox);
        footer_survey = findViewById(R.id.footer_survey);
        footer_home_label = findViewById(R.id.footer_home_label);
        footer_doctors_label = findViewById(R.id.footer_doctors_label);
        footer_donate_label = findViewById(R.id.footer_donate_label);
        footer_moneybox_label = findViewById(R.id.footer_moneybox_label);
        footer_survey_label = findViewById(R.id.footer_survey_label);
        header_title = findViewById(R.id.header_title);
        header_dots = findViewById(R.id.header_dots);
        lang = findViewById(R.id.lang);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkper(0);

    }
public void checkper(int page){
    Dexter.withActivity(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(new PermissionListener() {
                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {

                    Dexter.withActivity(MainActivity.this)
                            .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                            // TODO: Consider calling
                                            //    Activity#requestPermissions
                                            // here to request the missing permissions, and then overriding
                                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            //                                          int[] grantResults)
                                            // to handle the case where the user grants the permission. See the documentation
                                            // for Activity#requestPermissions for more details.
                                            return;
                                        }
                                    }
                                    Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (lastKnownLocationGPS != null) {
                                        SharedHelper.putKey(context,SharedHelper.lat,String.valueOf(lastKnownLocationGPS.getLatitude()));
                                        SharedHelper.putKey(context,SharedHelper.lng,String.valueOf(lastKnownLocationGPS.getLongitude()));
                                    }
                                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, MainActivity.this);
                                    if(page==1)
                                        showpopupweather();
                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {


                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).check();
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {

                }

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }).check();
}
    @Override
    public void onLocationChanged(Location location) {
        SharedHelper.putKey(context,SharedHelper.lat,String.valueOf(location.getLatitude()));
        SharedHelper.putKey(context,SharedHelper.lng,String.valueOf(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    public class AppWebViewClients extends WebViewClient {
        private ProgressBar progressBar;

        public AppWebViewClients(ProgressBar progressBar) {
            this.progressBar=progressBar;
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }
}
