package co.zamtech.mmt;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.internal.$Gson$Preconditions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.zamtech.mmt.MySqlite.AlarmsModel;
import co.zamtech.mmt.MySqlite.RemindersModel;
import co.zamtech.mmt.VolleyClasses.MySingleton;
import co.zamtech.mmt.models.NameLisModel;
import co.zamtech.utilities.Utilities;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.internal.Util;


public class Tab3 extends Fragment {

    private Spinner colorSpinner;
    private Spinner whichEyeSpinner;
    private TextView startDateTextView;
    private Spinner numberOfDaysSpinner;
    private Spinner howOftenSpinner;
    private Spinner tapperDrops1Spinner;
    private Spinner tapperDrops2Spinner;
    private Button addButton;
    private GridLayout gridLayout;
    private ArrayList<TextView> alarmsTextViewArray;
    private Calendar alarmToSetCalender;
    private int iNumberOfDay;
    private int ihowOften;
    private int[][] hourAndMinutes;
    private ArrayList<NameLisModel> nameLisModels;
    RemindersModel remindersModel;
    AlarmsModel alarmsModel;
    int reminderId;
    int alarmLastId;
    List<String> names;
    Spinner spCategories;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static final String TAG = "MainActivity";
    boolean photo_chnage = false;
    Uri uri = null;
    String imageEncoded, writeText;
    String photo1 = "";
    String check_img = "";
    Bitmap selected_img_bitmap;
    Boolean photoChange = false;
    int user_id, id;
    CircleImageView profile_image;
    public static final int REQUEST_IMAGE = 100;
    String input = "";
    String getName = "";
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        alarmToSetCalender = Calendar.getInstance();
        // Inflate the layout for this fragment
        View inf = inflater.inflate(R.layout.fragment_tab3, container, false);
        //Toast.makeText(getActivity(),"tab3!",Toast.LENGTH_SHORT).show();
        alarmsTextViewArray = new ArrayList<>();
//        nameEditText = (EditText)inf.findViewById(R.id.reminder_name);
        colorSpinner = (Spinner) inf.findViewById(R.id.color);
        startDateTextView = (TextView) inf.findViewById(R.id.start_date);
        whichEyeSpinner = (Spinner) inf.findViewById(R.id.which_eye);
        numberOfDaysSpinner = (Spinner) inf.findViewById(R.id.number_of_days);
        spCategories = (Spinner) inf.findViewById(R.id.spCategories);
        howOftenSpinner = (Spinner) inf.findViewById(R.id.how_often);
        profile_image = inf.findViewById(R.id.profile_image);
        gridLayout = (GridLayout) inf.findViewById(R.id.grid_layout);
        Calendar time = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        remindersModel = new RemindersModel(getContext());

        alarmsModel = new AlarmsModel(getContext());
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             launchCameraIntent();
            }
        });
        howOftenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gridLayout.removeAllViews();
                alarmsTextViewArray.clear();
                for (int i = 0; i <= position; i++) {
                    TextView alarm = new TextView(getActivity());
                    alarm.setTextSize(15);
                    time.add(Calendar.HOUR, i);
//                    alarm.setText(time.get(Calendar.HOUR) +":"+ time.get(Calendar.MINUTE)   + "> ");
                    alarm.setText("08:00 AM >  ");//sdf.format(time.getTime()) + " >  "
                    alarm.setTextColor(Color.BLACK);


                    alarm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                    if (selectedHour > 12) {

                                        //alarm.setText(String.valueOf(selectedHour-12)+ ":"+(String.valueOf(minute)+"pm"));
                                        time.set(Calendar.HOUR_OF_DAY, selectedHour - 12);
                                        time.set(Calendar.MINUTE, selectedMinute);
                                        alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                                    }
                                    if (selectedHour == 12) {
                                        //alarm.setText("12"+ ":"+(String.valueOf(minute)+"pm"));
                                        time.set(Calendar.HOUR_OF_DAY, 12);
                                        time.set(Calendar.MINUTE, selectedMinute);
                                        alarm.setText(sdf.format(time.getTime()) + " PM >  ");
                                    }
                                    if (selectedHour < 12) {
                                        //alarm.setText(String.valueOf(selectedHour)+ ":"+(String.valueOf(minute)+"am"));
                                        time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                        time.set(Calendar.MINUTE, selectedMinute);
                                        alarm.setText(sdf.format(time.getTime()) + " AM >  ");
                                    }

                                    /*time.set(Calendar.HOUR_OF_DAY, selectedHour);
                                    time.set(Calendar.MINUTE, selectedMinute);
                                    alarm.setText(sdf.format(time.getTime()) +  " >  ");*/


                                }
                            }, hour, minute, false);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();
                        }
                    });
                    gridLayout.addView(alarm);
                    alarmsTextViewArray.add(alarm);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tapperDrops1Spinner = (Spinner) inf.findViewById(R.id.taper_drop1);//number of drop
        tapperDrops2Spinner = (Spinner) inf.findViewById(R.id.taper_drop2);//frequency of drops
        addButton = (Button) inf.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String name = nameEditText.getText().toString();
                if (!getName.equals("")) {
                    if (isAlarmsSame()) {
                        Toast.makeText(getContext(), "Alarms cannot be same !", Toast.LENGTH_SHORT).show();
                    } else {
                        String color = colorSpinner.getSelectedItem().toString();
                        String startDate = startDateTextView.getText().toString();
                        String whichEye = whichEyeSpinner.getSelectedItem().toString();
                        String sNumberOfDay = numberOfDaysSpinner.getSelectedItem().toString();
                        if (sNumberOfDay.equals("OnGoing")) {
                            iNumberOfDay = -1;
                        } else {
                            iNumberOfDay = numberOfDaysSpinner.getSelectedItemPosition() + 1;
                        }
                        ihowOften = howOftenSpinner.getSelectedItemPosition() + 1;
                        int drop = tapperDrops1Spinner.getSelectedItemPosition();
                        int dropFrequency = tapperDrops2Spinner.getSelectedItemPosition() + 1;

                        hourAndMinutes = new int[ihowOften][2];

                        RemindersModel remindersModel = new RemindersModel(getContext());
                        //adding reminder to DB
                        remindersModel.insertReminders(getName, startDate, color, whichEye, iNumberOfDay, ihowOften,
                                drop, dropFrequency);
                        Log.e("reminder id before", String.valueOf(reminderId));
                        reminderId = remindersModel.getLastRecordId();
                        Log.e("reminder id after", String.valueOf(reminderId));

                        if (Tab2.reminderArrayList != null) {
                            Tab2.addReminderToArrayList(new Reminder(reminderId, getName, startDate, color, whichEye, iNumberOfDay, ihowOften,
                                    drop, dropFrequency));
                            //Toast.makeText(getContext() ,"not null",Toast.LENGTH_SHORT).show();
                        } else {
                            Tab2.reminderArrayList = new ArrayList<>();
                            Tab2.addReminderToArrayList(new Reminder(reminderId, getName, startDate, color, whichEye, iNumberOfDay, ihowOften,
                                    drop, dropFrequency));
                            //Toast.makeText(getContext() ,"null",Toast.LENGTH_SHORT).show();
                        }


                        Toast.makeText(getContext(), "Reminder Added Successfully !", Toast.LENGTH_SHORT).show();

                        createAlarmsIntents();
//                        nameEditText.setText("");
                        Intent intent = new Intent(getContext(), EyeDropAlarmTest.class);
                        startActivity(intent);
                    }


                } else {
                    Toast.makeText(getContext(), "Please Enter Drop Name", Toast.LENGTH_SHORT).show();
                }


                /*Intent intent2 = new Intent(getContext(), EditEyeDropReminderActivity.class);
                startActivity(intent2); */
            }

        });//add button


        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        startDateTextView.setText(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
        DatePickerDialog ab = new DatePickerDialog(inf.getContext(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                alarmToSetCalender.set(Calendar.YEAR, year);
                alarmToSetCalender.set(Calendar.MONTH, month);
                alarmToSetCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDateTextView.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(month) + "/" + String.valueOf(year));

            }
        }, year, month, day);
        ab.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.show();
            }
        });
        getNamesListApi();
        return inf;
    }//on create

    private void saveReminderAndAlarm() {

    }

    private void getNamesListApi() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "https://muhammadimedicaltrust.org.pk/admin_panel/api/drops.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    nameLisModels = new ArrayList<NameLisModel>();
                    JSONArray object = new JSONArray(response);
                    names = new ArrayList<>();
                    JSONObject jsonObject;
                    for (int i = 0; i < object.length(); i++) {
                        jsonObject = object.getJSONObject(i);
                        int id = jsonObject.getInt("id");
                        String name = jsonObject.getString("name");
                        nameLisModels.add(new NameLisModel(id, name));
                        names.add(name);

                    }


                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, names);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spCategories.setAdapter(dataAdapter);
//                        categories.add(0, "Select Category");
//                        spCategories.setSelection(0);
                    spCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            getName = parent.getItemAtPosition(position).toString();
//                                cat_id = vendorCategoryModels.get(position).getId();
//                                getSubCategoriesApi(String.valueOf(cat_id));
                            //  Toast.makeText(getActivity(), getName, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

//                params here
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);


    }

    private void createAlarmsIntents() {

        AlarmManager mgrAlarm = (AlarmManager) getContext().getSystemService(getContext().ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        AlarmsModel alarmsModel = new AlarmsModel(getContext());
        int requestCode = 0;//alarmsModel.getLastRequestCode();

        int hours, minutes;
        String AM_PM;
        for (int i = 0; i < iNumberOfDay; i++) {
            alarmToSetCalender.add(Calendar.DATE, i * ihowOften);
            for (int j = 0; j < alarmsTextViewArray.size(); j++) {
                ++requestCode;
                hours = Integer.parseInt((String) alarmsTextViewArray.get(j).getText().subSequence(0, 2));
                minutes = Integer.parseInt((String) alarmsTextViewArray.get(j).getText().subSequence(3, 5));
                AM_PM = (String) alarmsTextViewArray.get(j).getText().subSequence(6, 8);
                alarmToSetCalender.set(Calendar.HOUR_OF_DAY, hours);
                alarmToSetCalender.set(Calendar.MINUTE, minutes);

                if (AM_PM.equals("AM")) {
                    alarmToSetCalender.set(Calendar.AM_PM, Calendar.AM);
                } else {
                    alarmToSetCalender.set(Calendar.AM_PM, Calendar.PM);
                }

                alarmLastId = alarmsModel.getLastRecordId();
                if (alarmLastId == 0) {
                    alarmLastId = 1;
                }

                //values thek ja rahi hen
                alarmsModel.insertAlarms(alarmsTextViewArray.get(j).getText().toString(),
                        alarmLastId/*requestcode*/,
                        String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                                String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                                String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId);


                if (Tab1.alarmArrayList != null) {
                    Tab1.addAlarmToArrayList(new Alarm(alarmLastId, alarmsTextViewArray.get(j).getText().toString(), alarmLastId/*requestcode*/, true, String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId));
                    //Toast.makeText(getContext() ,"not null",Toast.LENGTH_SHORT).show();
                } else {
                    Tab1.alarmArrayList = new ArrayList<>();
                    Tab1.addAlarmToArrayList(new Alarm(alarmLastId, alarmsTextViewArray.get(j).getText().toString(), alarmLastId/*requestcode*/, true, String.valueOf(alarmToSetCalender.get(Calendar.DAY_OF_MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.MONTH)) + "/" +
                            String.valueOf(alarmToSetCalender.get(Calendar.YEAR)), "Active", reminderId));
                    //Toast.makeText(getContext() ,"null",Toast.LENGTH_SHORT).show();
                }

                Intent intent = new Intent(getContext(), AlarmReciever.class);
                intent.putExtra("reminderId", reminderId);
                // Loop counter `i` is used as a `requestCode`
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), alarmLastId/*requestcode*/, intent, 0);
                // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)

                mgrAlarm.set(AlarmManager.RTC_WAKEUP,
                        SystemClock.elapsedRealtime() + (
                                alarmToSetCalender.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()), pendingIntent);
                //Toast.makeText(getContext() ,String.valueOf((SystemClock.elapsedRealtime() +(
                //       alarmToSetCalender.getTimeInMillis()-Calendar.getInstance().getTimeInMillis()))/1000),Toast.LENGTH_SHORT).show();
                intentArray.add(pendingIntent);
                //Log.e("Alarm -----------------",String.valueOf((alarmToSetCalender.getTimeInMillis()/1000)/60));


            }
        }

    }

    boolean isAlarmsSame() {
        int x;

        for (int i = 0; i < alarmsTextViewArray.size(); i++) {
            x = 0;
            for (int j = 0; j < alarmsTextViewArray.size(); j++) {
                if (alarmsTextViewArray.get(i).getText().equals(alarmsTextViewArray.get(j).getText())) {
                    x++;
                }
            }
            if (x >= 2) {
                return true;
            }
        }
        return false;
    }


    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(profile_image);
        profile_image.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));
    }

    private void onProfileImageClick() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(getActivity(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {

                photoChange = true;
                uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                    // loading profile image from local cache
                     input=uri.toString();
                    Utilities.saveString(getActivity(),"imageUri",input);
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // SignupFragment.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    1
            );
        }
    }
}