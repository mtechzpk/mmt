package co.zamtech.mmt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingHowToUseEyeDropActivity extends AppCompatActivity {

    private ImageView aheader_back;
    private TextView aheader_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_how_to_use_eye_drop);
        aheader_back=findViewById(R.id.aheader_back);
        aheader_back.setOnClickListener(v -> {
            finish();
        });
    }
}