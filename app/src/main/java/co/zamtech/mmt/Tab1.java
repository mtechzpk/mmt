package co.zamtech.mmt;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.zamtech.mmt.Adapter.RemindersListViewAdapter;
import co.zamtech.mmt.Adapter.ScheduleListViewAdapter;


public class Tab1 extends Fragment {


    public static ArrayList<Alarm> alarmArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(alarmArrayList  == null){
            alarmArrayList = new ArrayList<>();
        }


        View inf = inflater.inflate(R.layout.fragment_tab1, container, false);
        ListView listView = (ListView) inf.findViewById(R.id.schedule_list_view);

        Alarm alarm = new Alarm();
        alarmArrayList = alarm.getAlarms(getContext());
        ScheduleListViewAdapter adapter = new ScheduleListViewAdapter(this.getContext(), R.layout.schedule_as_view, alarmArrayList);
        listView.setAdapter(adapter);
        // Inflate the layout for this fragment
        //Toast.makeText(getActivity(),"tab2!",Toast.LENGTH_SHORT).show();
        return inf;
    }

    public static void addAlarmToArrayList(Alarm alarm){
        alarmArrayList.add(alarm);

    }
}