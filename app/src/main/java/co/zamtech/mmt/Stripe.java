package co.zamtech.mmt;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Stripe extends AppCompatActivity {
    private Context context;
    private Activity activity;
    private WebView browser;
    private ProgressBar webviewloading;
    ImageView aheader_back;
    TextView aheader_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe);
        context=this;
        activity=this;
        aheader_back=findViewById(R.id.aheader_back);
        aheader_title=findViewById(R.id.aheader_title);
        aheader_title.setText("Donate Us");
        aheader_back.setOnClickListener(v -> {
            finish();
        });
        webviewloading=findViewById(R.id.webviewloading);
        browser =  findViewById(R.id.webview);
        browser.setWebViewClient(new AppWebViewClients(webviewloading));
        browser.getSettings().setLoadsImagesAutomatically(true);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        browser.loadUrl(context.getResources().getString(R.string.baseURLWebview)+"mobiledonation.php");

    }
    public class AppWebViewClients extends WebViewClient {
        private ProgressBar progressBar;

        public AppWebViewClients(ProgressBar progressBar) {
            this.progressBar=progressBar;
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }
}
