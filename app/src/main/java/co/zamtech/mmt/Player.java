package co.zamtech.mmt;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.Objects;

public class Player extends AppCompatActivity {
    PlayerView player_view;
    private SimpleExoPlayer mPlayer;
    private ProgressBar progressView;
    private String mVideoUrl;
    Context context;
    private long mCurrentMillis;

    @Override
    protected void onPause() {
        super.onPause();
        release();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoplayer);
        player_view=findViewById(R.id.player_view);
        progressView=findViewById(R.id.progressView);
        mVideoUrl= Objects.requireNonNull(getIntent().getExtras()).getString("videourl");
        context=this;


    }

    @Override
    protected void onResume() {
        super.onResume();
        startPlayer();
    }

    private void startPlayer() {
        if (mPlayer != null) {
            // no need to continue creating the player, if it's probably there
            return;
        }
        // set default options for the player
        /*mPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context),
                new DefaultTrackSelector());*/
        mPlayer = ExoPlayerFactory.newSimpleInstance(context);
        mPlayer.addListener(new com.google.android.exoplayer2.Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if(playbackState==3)
                    progressView.setVisibility(View.GONE);
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        player_view.setUseController(true);
        player_view.setPlayer(mPlayer);
        //player_view.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

        DefaultDataSourceFactory dataSourceFactory =
                new DefaultDataSourceFactory(context, Util.getUserAgent(context, "player"));
        ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(mVideoUrl));

        // now is the more important part. here we check to see if we want to resume, or start from the beggining
        boolean isResuming = mCurrentMillis != 0;
        mPlayer.prepare(extractorMediaSource, isResuming, false);
        mPlayer.setRepeatMode(com.google.android.exoplayer2.Player.REPEAT_MODE_ONE);

        mPlayer.setPlayWhenReady(true);
        if (isResuming) {
            // want to resume? seek to the old position
            mPlayer.seekTo(mCurrentMillis);
        }

    }
    private void release() {
        if (mPlayer == null) {
            return;
        }
        // save current position and release player
        mCurrentMillis = mPlayer.getCurrentPosition();
        mPlayer.release();
        mPlayer = null;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //First Hide other objects (listview or recyclerview), better hide them using Gone.
            //getActionBar().hide();
            //getSupportActionBar().hide();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) player_view.getLayoutParams();
            params.width=params.MATCH_PARENT;
            params.height=params.MATCH_PARENT;
            player_view.setLayoutParams(params);


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //unhide your objects here.
            //getSupportActionBar().show();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) player_view.getLayoutParams();
            params.width=params.MATCH_PARENT;
            params.height=600;
            player_view.setLayoutParams(params);


        }
    }
}
