package co.zamtech.mmt;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import co.zamtech.mmt.Adapter.RemindersListViewAdapter;


public class Tab2 extends Fragment {

    public static ArrayList<Reminder> reminderArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (reminderArrayList == null){
            reminderArrayList = new ArrayList<>();
        }
        View inf = inflater.inflate(R.layout.fragment_tab2, container, false);
        ListView listView = (ListView) inf.findViewById(R.id.list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * Callback method to be invoked when an item in this AdapterView has
             * been clicked.
             * <p>
             * Implementers can call getItemAtPosition(position) if they need
             * to access the data associated with the selected item.
             *
             * @param parent   The AdapterView where the click happened.
             * @param view     The view within the AdapterView that was clicked (this
             *                 will be a view provided by the adapter)
             * @param position The position of the view in the adapter.
             * @param id       The row id of the item that was clicked.
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), EditEyeDropReminderActivity.class);
                intent.putExtra("reminderID", reminderArrayList.get(position).getId());
                startActivity(intent);
            }
        });

        Reminder reminder = new Reminder();
        reminderArrayList = reminder.getReminders(getContext());


        RemindersListViewAdapter adapter = new RemindersListViewAdapter(this.getContext(), R.layout.remiders_as_view, reminderArrayList);
        listView.setAdapter(adapter);
        // Inflate the layout for this fragment
        //Toast.makeText(getActivity(),"tab2!",Toast.LENGTH_SHORT).show();
        return inf;
    }

    public static void addReminderToArrayList(Reminder reminder){
        reminderArrayList.add(reminder);

    }
}