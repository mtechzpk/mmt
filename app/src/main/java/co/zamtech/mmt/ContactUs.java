package co.zamtech.mmt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import co.zamtech.mmt.Helper.SharedHelper;

public class ContactUs extends AppCompatActivity {
    Context context;
    Activity activity;
    ImageView img_phone,img_mail,img_web;
    TextView contactus_label,contactus_label2;
    ImageView aheader_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        context = this;
        activity = this;
        aheader_back=findViewById(R.id.aheader_back);
        aheader_back.setOnClickListener(v -> {
            finish();
        });
        img_phone=findViewById(R.id.img_phone);
        img_mail=findViewById(R.id.img_mail);
        img_web=findViewById(R.id.img_web);
        contactus_label=findViewById(R.id.contactus_label);
        contactus_label2=findViewById(R.id.contactus_label2);
        if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
            contactus_label.setText("ہم سے رابطہ کریں ");
            contactus_label2.setText("ہماری ٹیم جلد ہی آپ سے رابطہ کرے گی ");
        }
        img_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+9242376155556", null));
                startActivity(intent);
            }
        });
        img_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","hrrtrust@gmail.com", null));
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });
        img_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://muhammadimedicaltrust.org.pk"));
                startActivity(browserIntent);
            }
        });
    }
}
