package co.zamtech.mmt;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

import co.zamtech.mmt.MySqlite.EyeDropDatabaseHelper;
import co.zamtech.mmt.ui.main.SectionsPagerAdapter;

public class EyeDropAlarmTest extends AppCompatActivity {

    private ImageView aheader_back;
    private ImageView aheader_setting;
    private TextView aheader_title;
    private TextView start_date;
    private Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_eye_drop_alarm_test);
        aheader_back=findViewById(R.id.aheader_back);
        aheader_setting=findViewById(R.id.setting);
        aheader_setting.setOnClickListener(v -> {
            Intent intent = new Intent(EyeDropAlarmTest.this, SettingEyeDropActivity.class);
            startActivity(intent);
        });
        aheader_title=findViewById(R.id.aheader_title);
        aheader_title.setText("Eye Drop Alarm");

        aheader_back.setOnClickListener(v -> {
            finish();
        });

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        //spinner = (Spinner) findViewById(R.id.coloursSpinner);


    }



}