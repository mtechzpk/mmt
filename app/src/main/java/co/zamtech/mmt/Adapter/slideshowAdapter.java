package co.zamtech.mmt.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.zamtech.mmt.R;

public class slideshowAdapter extends PagerAdapter {
    private Context ctx;
    private ArrayList<String> slider=new ArrayList<>();
    public slideshowAdapter(Context ctx, ArrayList<String> msSlider) {
        this.ctx = ctx;
        this.slider=msSlider;
    }

    @Override
    public int getCount() {
        return slider.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View v= inflater.inflate(R.layout.custom_view_slideshow, null);
        ImageView img=(ImageView) v.findViewById(R.id.slideimage);
        //img.setImageResource(images[position]);
        img.setScaleType(ImageView.ScaleType.FIT_XY);
        //img.setAdjustViewBounds(true);
        Picasso.get().load(ctx.getResources().getString(R.string.baseURLImage)+slider.get(position)).placeholder(R.drawable.black).into(img);
        ViewPager vp=(ViewPager) container;
        vp.addView(v,0);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp=(ViewPager) container;
        View ve=(View) object;
        vp.removeView(ve);
    }
}
