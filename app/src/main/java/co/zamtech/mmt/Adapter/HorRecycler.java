package co.zamtech.mmt.Adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.shadow.ShadowRenderer;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.MainActivity;
import co.zamtech.mmt.R;
import co.zamtech.mmt.SectionDetail;
import co.zamtech.mmt.SeeAllSection;
import co.zamtech.mmt.Splash;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class HorRecycler extends RecyclerView.Adapter<HorRecycler.ViewHolder> {
    private ArrayList<String> ID;
    private ArrayList<String> Type;
    private ArrayList<String> Title;
    private ArrayList<String> Video;
    private ArrayList<String> Image;
    private Context mContext;
    private String Page; //1-news, 2-events, 3-post, 4-video, 5-research
    private static final int CONTENT_TYPE = 220;
    private static final int CONTENT_SEEALL = 110;


    public HorRecycler(ArrayList<String> msID, ArrayList<String> msType, ArrayList<String> msTitle, ArrayList<String> msVideo, ArrayList<String> msImage, String msPage, Context msContext) {
        this.ID = msID;
        this.Type = msType;
        this.Title = msTitle;
        this.Video = msVideo;
        this.Image = msImage;
        this.Page=msPage;
        this.mContext = msContext;
    }

    @NonNull
    @Override
    public HorRecycler.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == CONTENT_TYPE) {
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_hor,parent,false);
        }else{
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_seeall,parent,false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HorRecycler.ViewHolder viewHolder, final int position) {
        int viewType = getItemViewType(position);
        if (viewType == CONTENT_TYPE) {
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                try {
                    viewHolder.title.setText(new String(Title.get(position).getBytes("ISO-8859-1"), "utf-8"));
                }catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }else{
                viewHolder.title.setText(Title.get(position));
            }

            if(Image.get(position).equals("")){
                if(!Video.get(position).equals("")){
                    viewHolder.play.setVisibility(View.VISIBLE);
                }
            }

            Glide.with(mContext).load(mContext.getResources().getString(R.string.baseURLImage) + Image.get(position)).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).placeholder(R.drawable.black).into(viewHolder.thumbnail);
            viewHolder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, SectionDetail.class);
                    intent.putExtra("secid",ID.get(position));
                    mContext.startActivity(intent);
                }
            });
        }else{
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                viewHolder.seeall_title.setText("تمام دیکھیں");
            }
            viewHolder.seeall_card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pagename;
                    switch (Page) {
                        case "1":
                            if(SharedHelper.getKey(mContext, SharedHelper.lang).equals("1")){
                                pagename = "خبریں";
                            }else{
                                pagename = "NEWS";
                            }
                            break;
                        case "2":
                            if(SharedHelper.getKey(mContext, SharedHelper.lang).equals("1")){
                                pagename = "تقریب";
                            }else{
                                pagename = "EVENTS";
                            }
                            break;
                        case "3":
                            if(SharedHelper.getKey(mContext, SharedHelper.lang).equals("1")){
                                pagename = "پوسٹ";
                            }else{
                                pagename = "POSTS";
                            }
                            break;
                        case "4":
                            if(SharedHelper.getKey(mContext, SharedHelper.lang).equals("1")){
                                pagename = "ویڈیوز";
                            }else{
                                pagename = "VIDEOS";
                            }
                            break;
                        default:
                            if(SharedHelper.getKey(mContext, SharedHelper.lang).equals("1")){
                                pagename = "تحقیق";
                            }else{
                                pagename = "RESEARCH";
                            }
                            break;
                    }
                    Intent intent=new Intent(mContext, SeeAllSection.class);
                    intent.putExtra("secid",Page);
                    intent.putExtra("secname", pagename);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (ID.size()+1);

        //return mTitle.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position==ID.size()){
            return CONTENT_SEEALL;
        }else{
            return CONTENT_TYPE;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,seeall_title;
        ImageView thumbnail,play,seeall_thumbnail;
        CardView card_view,seeall_card_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            play = itemView.findViewById(R.id.play);
            card_view = itemView.findViewById(R.id.card_view);
            seeall_title = itemView.findViewById(R.id.seeall_title);
            seeall_card_view = itemView.findViewById(R.id.seeall_card_view);
            seeall_thumbnail = itemView.findViewById(R.id.seeall_thumbnail);
        }

    }
}
