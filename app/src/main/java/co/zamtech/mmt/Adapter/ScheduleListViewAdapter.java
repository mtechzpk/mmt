package co.zamtech.mmt.Adapter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Calendar;

import co.zamtech.mmt.Alarm;
import co.zamtech.mmt.AlarmReciever;
import co.zamtech.mmt.R;
import co.zamtech.mmt.Reminder;
import co.zamtech.mmt.Tab1;

import static java.util.Calendar.AM_PM;

public class ScheduleListViewAdapter extends ArrayAdapter<Alarm> {
    Context context;
    int resource;
    ArrayList<Alarm> alarmArrayList;
    Intent intent;
    Reminder reminder;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;
    private Calendar alarmToSetCalender;
    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     */

    public ScheduleListViewAdapter(@NonNull Context context, int resource, ArrayList<Alarm> alarmArrayList) {
        super(context, resource, alarmArrayList);
        this.context = context;
        this.resource = resource;
        this.alarmArrayList = alarmArrayList;
        intent = new Intent(getContext(), AlarmReciever.class);
        alarmManager = (AlarmManager)getContext().getSystemService(getContext().ALARM_SERVICE);
        alarmToSetCalender = Calendar.getInstance();
        reminder = new Reminder();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =  LayoutInflater.from(context);

        View view = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.schedule_as_view, null);

        TextView nameTextView = (TextView)view.findViewById(R.id.schedule_name);
        TextView whichEyeTextView = (TextView)view.findViewById(R.id.which_eye_S);
        TextView timeTextView = (TextView)view.findViewById(R.id.time);
        TextView dateTextView = (TextView)view.findViewById(R.id.date);
        CheckBox disableAlarmCheckBox = (CheckBox)view.findViewById(R.id.checkbox);
        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.schedule_layout);

        reminder = reminder.getRemindersById(getContext(), alarmArrayList.get(position).getReminderId());
        if(reminder != null){
            nameTextView.setText(reminder.getName());
            whichEyeTextView.setText(reminder.getWhichEye() + " Eye");
            timeTextView.setText(alarmArrayList.get(position).getTime().subSequence(0,8));
            dateTextView.setText(alarmArrayList.get(position).getAlarmDate());

            if(!alarmArrayList.get(position).getEnableAlarm()){
                disableAlarmCheckBox.setChecked(true);
                linearLayout.setBackgroundColor(Color.DKGRAY);
            }
            //Toast.makeText(view.getContext(),reminder.getWhichEye(), Toast.LENGTH_SHORT).show();
        }
        disableAlarmCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(disableAlarmCheckBox.isChecked()){
                    alarmArrayList.get(position).disableAlarm(context, alarmArrayList.get(position).getId());//disable the alarm set true in DB
                    pendingIntent = PendingIntent.getBroadcast(context, alarmArrayList.get(position).getRequestCode(), intent,0);
                    alarmManager.cancel(pendingIntent);
                    pendingIntent.cancel();

                    Tab1.alarmArrayList.get(position).setEnableAlarm(false);
                    linearLayout.setBackgroundColor(Color.DKGRAY);
                }else {
                    alarmArrayList.get(position).enableAlarm(context, alarmArrayList.get(position).getId());//enable the alarm set true in DB
                    pendingIntent = PendingIntent.getBroadcast(context, alarmArrayList.get(position).getRequestCode(), intent,0);


                    int hours = Integer.parseInt((String) alarmArrayList.get(position).getTime().subSequence(0,2));
                    int minutes = Integer.parseInt((String) alarmArrayList.get(position).getTime().subSequence(3,5));
                    String AM_PM = (String) alarmArrayList.get(position).getTime().subSequence(6,8);
                    alarmToSetCalender.set(Calendar.HOUR_OF_DAY, hours);
                    alarmToSetCalender.set(Calendar.MINUTE, minutes);


                    int year = Integer.parseInt((String)alarmArrayList.get(position).getAlarmDate().split("[/]")[2]);
                    int month = Integer.parseInt((String)alarmArrayList.get(position).getAlarmDate().split("[/]")[1]);
                    int dayOfMonth = Integer.parseInt((String) alarmArrayList.get(position).getAlarmDate().split("[/]")[0]);

                    alarmToSetCalender.set(Calendar.YEAR, year);
                    alarmToSetCalender.set(Calendar.MONTH, month);
                    alarmToSetCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    if(AM_PM.equals("AM")){
                        alarmToSetCalender.set(Calendar.AM_PM, Calendar.AM);
                    }else {
                        alarmToSetCalender.set(Calendar.AM_PM, Calendar.PM);
                    }
                    //intent = new Intent(getContext(), AlarmReciever.class);
                    intent.putExtra("reminderId", alarmArrayList.get(position).getReminderId());
                    Log.e("Position",String.valueOf(position));
                    Log.e("requestcode",String.valueOf(alarmArrayList.get(position).getRequestCode()));
                    pendingIntent = PendingIntent.getBroadcast(getContext(), alarmArrayList.get(position).getReminderId(), intent, 0);

                    alarmManager.set(alarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime() +(
                                    alarmToSetCalender.getTimeInMillis()- Calendar.getInstance().getTimeInMillis()), pendingIntent);
                    Tab1.alarmArrayList.get(position).setEnableAlarm(true);
                    linearLayout.setBackgroundColor(Color.WHITE);
                }
            }
        });//disableAlarmCheckBox




        //

        return view;

    }
}
