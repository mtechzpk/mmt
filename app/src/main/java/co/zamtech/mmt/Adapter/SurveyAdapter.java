package co.zamtech.mmt.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.server.sSurveySend;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder> {
    ArrayList<String> mId;
    ArrayList<String> mQuestion;
    ArrayList<String> mLevel;
    private Context mContext;
    private static final int CONTENT_TYPE = 220;
    private static final int BUTTON_TYPE = 120;
    public SurveyAdapter(ArrayList<String> msId,ArrayList<String> msQuestion, ArrayList<String> msLevel, Context msContext) {
        this.mId = msId;
        this.mQuestion = msQuestion;
        this.mLevel = msLevel;
        this.mContext = msContext;
    }
    @NonNull
    @Override
    public SurveyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=null;
        if (i == CONTENT_TYPE)
        {
            view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_row_survey,viewGroup,false);

        }else{
            view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_row_button,viewGroup,false);
        }

        return new SurveyAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SurveyAdapter.ViewHolder viewHolder, final int i) {
        int viewType = getItemViewType(i);
        if (viewType == CONTENT_TYPE) {
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                try{

                    viewHolder.question.setText(new String(mQuestion.get(i).getBytes("ISO-8859-1"), "utf-8"));
                    viewHolder.answer.setHint("اپنا جواب یہاں ٹائپ کریں");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                }else{
                    viewHolder.question.setText(mQuestion.get(i));
                }
            viewHolder.answer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.toString().trim().equals("") || s.toString().trim().isEmpty() || s.toString().trim().equals(" ")){
                        mLevel.set(i,"0");
                    }else{
                        mLevel.set(i,s.toString().trim());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else{
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                    viewHolder.submit_button.setText("جمع کرائیں");
            }
            viewHolder.submit_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean proceed=true;
                    for (int j=0;j<mLevel.size();j++){
                        if(proceed){
                            if(mLevel.get(j).equals("0")){
                                proceed=false;
                            }
                        }
                    }
                    if(proceed){
                        @SuppressLint("StaticFieldLeak") final sSurveySend sss=new sSurveySend(){
                            @Override
                            protected void onPostExecute(String s) {
                                Log.i("checkzamar", s);
                                if(s.equals("error") || s.equals("Unable to connect to internet")){
                                    Log.d("check", "onPostExecute: No internet");
                                    Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                                    if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                                        viewHolder.submit_button.setText("جمع کرائیں");
                                    }else{
                                        viewHolder.submit_button.setText("Submit");
                                    }
                                    viewHolder.submit_button.setEnabled(true);
                                }else{
                                    try {
                                        JSONArray ja_main = new JSONArray(s);
                                        JSONObject jo_main;
                                        jo_main=ja_main.getJSONObject(0);
                                        if(jo_main.getString("response").equals("yes")){
                                            viewHolder.submit_button.setText("Done");
                                            viewHolder.submit_button.setEnabled(false);
                                            Toast.makeText(mContext,"Submitted",Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                                            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                                                viewHolder.submit_button.setText("جمع کرائیں");
                                            }else{
                                                viewHolder.submit_button.setText("Submit");
                                            }
                                            viewHolder.submit_button.setEnabled(true);
                                        }

                                    }catch (JSONException e){
                                        e.printStackTrace();
                                        Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                                        if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                                            viewHolder.submit_button.setText("جمع کرائیں");
                                        }else{
                                            viewHolder.submit_button.setText("Submit");
                                        }
                                        viewHolder.submit_button.setEnabled(true);
                                    }

                                }

                            }
                        };
                        JSONArray jsonArray = new JSONArray();
                        for (int j=0;j<mLevel.size();j++){
                            JSONObject obj = new JSONObject();
                            try {
                                obj.put("questionid", mId.get(j))
                                        .put("answer", mLevel.get(j));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsonArray.put(obj);
                        }
                        if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                            viewHolder.submit_button.setText("بھیجنا");
                        }else{
                            viewHolder.submit_button.setText("Sending");
                        }

                        viewHolder.submit_button.setEnabled(false);
                        Log.i("checkzamar", "onClick: "+SharedHelper.getKey(mContext,SharedHelper.userid)+" "+jsonArray.toString());
                        sss.execute(mContext.getString(R.string.baseURL)+"insertusersurvey.php", SharedHelper.getKey(mContext,SharedHelper.userid),jsonArray.toString());
                    }else{
                        Toast.makeText(mContext,"You must answer all the questions.",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return (mId.size()+1);
    }
    @Override
    public int getItemViewType(int position) {
        if(position==mId.size()){
            return BUTTON_TYPE;
        }else{
            return CONTENT_TYPE;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView question;
        EditText answer;
        ExtendedFloatingActionButton submit_button;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            question=itemView.findViewById(R.id.question);
            answer=itemView.findViewById(R.id.answer);
            submit_button=itemView.findViewById(R.id.submit_button);

        }
    }
}
