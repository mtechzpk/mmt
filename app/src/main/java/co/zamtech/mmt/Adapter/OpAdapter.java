package co.zamtech.mmt.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;

public class OpAdapter extends RecyclerView.Adapter<OpAdapter.ViewHolder> {
    private ArrayList<String> Name;
    private Context mContext;
    private static final int CONTENT_TYPE = 220;


    public OpAdapter(ArrayList<String> msName, Context msContext) {
        this.Name = msName;
        this.mContext = msContext;
    }

    @NonNull
    @Override
    public OpAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_operation,parent,false);
        return new OpAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OpAdapter.ViewHolder viewHolder, final int position) {
        int viewType = getItemViewType(position);
        if (viewType == CONTENT_TYPE) {
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")) {
                try {
                    viewHolder.title.setText(new String(Name.get(position).getBytes("ISO-8859-1"), "utf-8"));
                }catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }else{
                viewHolder.title.setText(Name.get(position));
            }

        }
    }

    @Override
    public int getItemCount() {
        return Name.size();
    }

    @Override
    public int getItemViewType(int position) {
        return CONTENT_TYPE;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
        }

    }
}
