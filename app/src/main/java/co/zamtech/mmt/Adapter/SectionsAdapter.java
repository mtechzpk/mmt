package co.zamtech.mmt.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;
import co.zamtech.mmt.SectionDetail;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SectionsAdapter extends BaseAdapter {
    private ArrayList<String> mID;
    private ArrayList<String> mType;
    private ArrayList<String> mTitle;
    private ArrayList<String> mVideo;
    private ArrayList<String> mImage;
    private Context mContext;
    private LayoutInflater inflter;
    public SectionsAdapter(ArrayList<String> msID,ArrayList<String> msType,ArrayList<String> msTitle,ArrayList<String> msVideo,ArrayList<String> msImage, Context msContext) {
        this.mID = msID;
        this.mType = msType;
        this.mTitle = msTitle;
        this.mVideo = msVideo;
        this.mImage = msImage;
        this.mContext = msContext;
        if(mContext!=null)
            inflter = (LayoutInflater.from(mContext));
    }
    @Override
    public int getCount() {
        return mID.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(final int i, View itemView, ViewGroup viewGroup) {
        itemView = inflter.inflate(R.layout.custom_row_hor_full, null);
        itemView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 500));
        final TextView title;
        final ImageView thumbnail,play;
        CardView card_view;
        title = itemView.findViewById(R.id.title);
        thumbnail = itemView.findViewById(R.id.thumbnail);
        card_view = itemView.findViewById(R.id.card_view);
        play = itemView.findViewById(R.id.play);
        if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")) {
            try {
                title.setText(new String(mTitle.get(i).getBytes("ISO-8859-1"), "utf-8"));
            }catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }else{
            title.setText(mTitle.get(i));
        }

        if(mImage.get(i).equals("")){
            if(!mVideo.get(i).equals("")){
                play.setVisibility(View.VISIBLE);
            }
        }

        Glide.with(mContext).load(mContext.getResources().getString(R.string.baseURLImage) + mImage.get(i)).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).placeholder(R.drawable.black).into(thumbnail);
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, SectionDetail.class);
                intent.putExtra("secid",mID.get(i));
                mContext.startActivity(intent);
            }
        });
        return itemView;
    }
}