package co.zamtech.mmt.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;
import com.stripe.android.stripe3ds2.transaction.v;

import java.util.ArrayList;
import java.util.List;

import co.zamtech.mmt.MySqlite.RemindersModel;
import co.zamtech.mmt.R;
import co.zamtech.mmt.Reminder;

public class RemindersListViewAdapter extends ArrayAdapter<Reminder> {

    Context context;
    int resource;
    ArrayList<Reminder> reminderArrayList;

    public RemindersListViewAdapter(@NonNull Context context, int resource, ArrayList<Reminder> reminderArrayList) {
        super(context, resource, reminderArrayList);
        this.context = context;
        this.resource = resource;
        this.reminderArrayList = reminderArrayList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =  LayoutInflater.from(context);

        View view = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.remiders_as_view, null);

        ImageView imageView = view.findViewById(R.id.drop_imageview);
        TextView nameTextView = (TextView)view.findViewById(R.id.reminder_name);
        TextView whichEyeTextView = (TextView)view.findViewById(R.id.which_eye_T);
        TextView startDateTextView = (TextView)view.findViewById(R.id.start_date_T);
        TextView numberOfDropsTextView = (TextView)view.findViewById(R.id.drop_frequency_T);
        TextView numberDaysTextView = (TextView)view.findViewById(R.id.number_of_days_T);
        Reminder reminder;

        reminder = reminderArrayList.get(position);

        //Toast.makeText(view.getContext(),"dsfsdfsdfsd!",Toast.LENGTH_SHORT).show();
        nameTextView.setText(reminder.getName());
        whichEyeTextView.setText(reminder.getWhichEye());
        startDateTextView.setText(reminder.getStartDate());
        numberOfDropsTextView.setText(String.valueOf(reminder.getNoOfDrops()));
        numberDaysTextView.setText(String.valueOf(reminder.getNumberOfDays()));
//        Picasso.get().load(reminder.getImage()).into(imageView);
        return view;

    }
}
