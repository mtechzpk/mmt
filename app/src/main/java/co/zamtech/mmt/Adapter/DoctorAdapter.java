package co.zamtech.mmt.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.R;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.ViewHolder> {
    ArrayList<String> mName;
    ArrayList<String> mImage;
    ArrayList<String> mOptype;
    private Context mContext;
    private static final int CONTENT_TYPE = 220;
    public DoctorAdapter(ArrayList<String> msName,ArrayList<String> msImage,ArrayList<String> msOptype,Context msContext) {
        this.mName = msName;
        this.mImage = msImage;
        this.mOptype = msOptype;
        this.mContext = msContext;
    }
    @NonNull
    @Override
    public DoctorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=null;
        if (i == CONTENT_TYPE)
        {
            view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_row_doctor,viewGroup,false);

        }

        return new DoctorAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DoctorAdapter.ViewHolder viewHolder, final int i) {
        int viewType = getItemViewType(i);
        if (viewType == CONTENT_TYPE) {
            //viewHolder.title.setTypeface(ResourcesCompat.getFont(mContext,R.font.urdu));
            //viewHolder.title.setTypeface(Typeface.createFromAsset(mContext.getAssets(),"font/urdu.ttf"));
            if(SharedHelper.getKey(mContext,SharedHelper.lang).equals("1")){
                try{
                    viewHolder.title.setText(new String(mName.get(i).getBytes("ISO-8859-1"), "utf-8"));
                    viewHolder.type.setText(new String(mOptype.get(i).getBytes("ISO-8859-1"), "utf-8"));
                    viewHolder.title.setGravity(Gravity.START);
                    viewHolder.type.setGravity(Gravity.START);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }else{
                String name=mOptype.get(i);
                name=name.replace("â\u0080¢","\u25CF");
                viewHolder.title.setText(mName.get(i));
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    viewHolder.type.setText(Html.fromHtml(mOptype.get(i), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    viewHolder.type.setText(Html.fromHtml(mOptype.get(i)));
                }*/
                viewHolder.type.setText(name);
            }
            Log.i("checkzamar", "onBindViewHolder: "+mContext.getResources().getString(R.string.baseURLImage) + mImage.get(i));
            Glide.with(mContext).load(mContext.getResources().getString(R.string.baseURLImage) + mImage.get(i)).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).placeholder(R.drawable.profileimage).into(viewHolder.thumbnail);
        }


    }

    @Override
    public int getItemCount() {
        return mName.size();
    }
    @Override
    public int getItemViewType(int position) {
        return CONTENT_TYPE;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView title,type;
        ImageView thumbnail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            type=itemView.findViewById(R.id.type);
            thumbnail=itemView.findViewById(R.id.thumbnail);

        }
    }
}
