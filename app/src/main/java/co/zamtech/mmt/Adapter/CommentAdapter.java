package co.zamtech.mmt.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import co.zamtech.mmt.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    ArrayList<String> mUsername;
    ArrayList<String> mComment;
    private Context mContext;
    private static final int CONTENT_TYPE = 220;
    public CommentAdapter(ArrayList<String> msUsername,ArrayList<String> msComment,Context msContext) {
        this.mUsername = msUsername;
        this.mComment = msComment;
        this.mContext = msContext;
    }
    @NonNull
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=null;
        if (i == CONTENT_TYPE)
        {
            view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_row_comment,viewGroup,false);

        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentAdapter.ViewHolder viewHolder, final int i) {
        int viewType = getItemViewType(i);
        if (viewType == CONTENT_TYPE) {
            viewHolder.name.setText(mUsername.get(i));
            viewHolder.comment.setText(mComment.get(i));
        }


    }

    @Override
    public int getItemCount() {
        return mUsername.size();
    }
    @Override
    public int getItemViewType(int position) {
        return CONTENT_TYPE;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name,comment;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            comment=itemView.findViewById(R.id.comment);

        }
    }
}
