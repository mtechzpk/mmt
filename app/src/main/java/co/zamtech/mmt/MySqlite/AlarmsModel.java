package co.zamtech.mmt.MySqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.Date;
import java.util.ArrayList;

import co.zamtech.mmt.Alarm;

public class AlarmsModel {
    private EyeDropDatabaseHelper dbHelper;

    private SQLiteDatabase database;

    public String stringArray[];

    public final static String ALARMS_TABLE = "alarms"; // name of table

    public final static String P_ID = "id"; // pid
    public final static String ALARM_TIME = "alarm_time"; // id value for food item
    public final static String REMINDER_ID = "reminder_id"; // name of food
    public final static String REQUEST_CODE = "request_code";
    public final static String ENABLE_ALARM = "enable_alarm";
    public final static String ALARM_DATE = "alarm_date";
    public final static String ALARM_STATUS = "alarm_status";
    /**
     *
     * @param context
     */
    public AlarmsModel(Context context) {
        dbHelper = new EyeDropDatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public long insertAlarms(String time, int requestCode, String date, String alarmStatus, int reminderId) {
        ContentValues values = new ContentValues();
        values.put(ALARM_TIME, time);
        values.put(REQUEST_CODE, requestCode);
        values.put(ALARM_DATE, date);
        values.put(ALARM_STATUS, alarmStatus);
        values.put(REMINDER_ID, reminderId);
        values.put(ENABLE_ALARM, 1);
        return database.insert(ALARMS_TABLE, null, values);
    }

    public Cursor selectAllAlarms() {
        String[] cols = new String[] { P_ID, ALARM_TIME, REQUEST_CODE, ENABLE_ALARM, ALARM_DATE, ALARM_STATUS, REMINDER_ID};
        Cursor mCursor = database.query(true, ALARMS_TABLE, cols, null,
                null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }

    public Cursor getAlarmsByReminderId(int reminderId) {
        //String[] cols = new String[] {P_ID, ALARM_TIME, REQUEST_CODE};
        Cursor mCursor = database.rawQuery("SELECT id, alarm_time, request_code, enable_alarm, " +
                "alarm_date, alarm_status, reminder_id FROM '" + ALARMS_TABLE + "' " +
                "WHERE reminder_id = "+reminderId+"", null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }

    public int getLastRequestCode() {
        int column1 = 0;
        Cursor mCursor = database.rawQuery("SELECT request_code FROM '" + ALARMS_TABLE + "' " +
                "ORDER BY id DESC LIMIT 1", null);
        if (mCursor.moveToFirst()) {
            do {
                // Passing values
                column1 = mCursor.getInt(2);
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        return column1;
    }

    public int getLastRecordId() {
        int column1 = 0;
        Cursor mCursor = database.rawQuery("SELECT id FROM '" + ALARMS_TABLE + "' " +
                "ORDER BY id DESC LIMIT 1", null);
        if (mCursor.moveToFirst()) {
            do {
                // Passing values
                column1 = mCursor.getInt(0);
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        return column1;
    }

    public void deleteAlarm(String id) {
        database.delete(ALARMS_TABLE, REMINDER_ID+"=?", new String[]{id});
    }

    public int enableAlarm(int id){
        ContentValues values=new ContentValues();
        values.put(ENABLE_ALARM, 1);
        return database.update(ALARMS_TABLE, values, P_ID+"= ?", new String[]{String.valueOf(id)});
    }

    public int disableAlarm(int id){
        ContentValues values=new ContentValues();
        values.put(ENABLE_ALARM, 0);
        return database.update(ALARMS_TABLE, values, P_ID+"= ?", new String[]{String.valueOf(id)});
    }

    public int updateAlarm(String time, int reminderId, int requestCode, String date, String id, String alarmStatus){
        ContentValues values = new ContentValues();
        values.put(ALARM_TIME, time);
        values.put(REMINDER_ID, reminderId);
        values.put(REQUEST_CODE, requestCode);
        values.put(ENABLE_ALARM, 1);
        values.put(ALARM_DATE, date);
        values.put(ALARM_STATUS, alarmStatus);
        return database.update(ALARMS_TABLE, values, P_ID+"=?", new String[]{id});
    }
}
