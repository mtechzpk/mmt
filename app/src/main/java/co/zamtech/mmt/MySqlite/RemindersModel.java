package co.zamtech.mmt.MySqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RemindersModel {

    private EyeDropDatabaseHelper dbHelper;

    private SQLiteDatabase database;

    public String stringArray[];

    public final static String REMINDERS_TABLE = "reminders"; // name of table

    public final static String P_ID = "id"; // pid
    public final static String REMINDER_NAME = "name"; // id value for food item
    public final static String REMINDER_START_DATE = "start_date"; // name of food
    public final static String REMINDER_COLOR = "color"; // quantity of food item
    public final static String REMINDER_WHICH_EYE = "which_eye"; // measurement of food item
    public final static String REMINDER_NO_OF_DAYS = "no_of_days"; // expiration date of food item
    public final static String REMINDER_HOW_OFTEN = "how_often"; //  date of food item added
    public final static String REMINDER_NO_OF_DROPS = "no_of_drops";
    public final static String REMINDER_DROP_FREQUENCY = "drop_frequency";
    /**
     *
     * @param context
     */
    public RemindersModel(Context context) {
        dbHelper = new EyeDropDatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public long insertReminders(String name, String startDate, String color, String whichEye,
                                int noOfDays, int howOften, int noOfDrops,
                                int dropFrequency) {
        ContentValues values = new ContentValues();
        values.put(REMINDER_NAME, name);
        values.put(REMINDER_START_DATE, startDate);
        values.put(REMINDER_COLOR, color);
        values.put(REMINDER_WHICH_EYE, whichEye);
        values.put(REMINDER_NO_OF_DAYS, noOfDays);
        values.put(REMINDER_HOW_OFTEN, howOften);
        values.put(REMINDER_NO_OF_DROPS, noOfDrops);
        values.put(REMINDER_DROP_FREQUENCY, dropFrequency);
        return database.insert(REMINDERS_TABLE, null, values);
    }

//    public long insertFoodItemsDetails(String id, String name,String quantity, String measurement, String currrentDate,String expiration) {
//        ContentValues values = new ContentValues();
//        values.put(FOOD_ID, id);
//        values.put(FOOD_NAME, name);
//        values.put(FOOD_QUANTITY, quantity);
//        values.put(FOOD_MEASUREMENT, measurement);
//        values.put(FOOD_CURRENTDATE, currrentDate);
//        values.put(FOOD_EXPIRATION, expiration);
//        return database.insert(FOOD_ITEMS_DETAILS, null, values);
//
//    }

    public Cursor getRemindersById(String id) {
        String[] cols = new String[] { P_ID, REMINDER_NAME, REMINDER_START_DATE, REMINDER_COLOR,
                REMINDER_WHICH_EYE, REMINDER_NO_OF_DAYS, REMINDER_HOW_OFTEN, REMINDER_NO_OF_DROPS,
                REMINDER_DROP_FREQUENCY};
        Cursor mCursor = database.query(true, REMINDERS_TABLE, cols, P_ID+"=?",
                new String[]{id}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }

    public Cursor getAllReminders() {
        String[] cols = new String[] { P_ID, REMINDER_NAME, REMINDER_START_DATE, REMINDER_COLOR,
                REMINDER_WHICH_EYE, REMINDER_NO_OF_DAYS, REMINDER_HOW_OFTEN, REMINDER_NO_OF_DROPS,
                REMINDER_DROP_FREQUENCY

        };
        Cursor mCursor = database.query(true, REMINDERS_TABLE, cols, null,
                null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }

    public int getLastRecordId() {
        int column1 = 0;
        Cursor mCursor = database.rawQuery("SELECT id FROM '" + REMINDERS_TABLE + "' " +
                "ORDER BY id DESC LIMIT 1", null);
        if (mCursor.moveToFirst()) {
            do {
                // Passing values
                column1 = mCursor.getInt(0);
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        return column1;
    }

//    public Cursor selectAllRecords(String loc) {
//        String[] cols = new String[] { FOOD_ID, FOOD_NAME, FOOD_QUANTITY, FOOD_MEASUREMENT, FOOD_EXPIRATION,FLAG,LOCATION,P_ID};
//        Cursor mCursor = database.query(true, FOOD_TABLE, cols, LOCATION+"=?", new String[]{loc}, null, null, null, null);
//        int size=mCursor.getCount();
//        stringArray = new String[size];
//        int i=0;
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//            FoodInfo.arrayList.clear();
//            while (!mCursor.isAfterLast()) {
//                String name=mCursor.getString(1);
//                stringArray[i]=name;
//                String quant=mCursor.getString(2);
//                String measure=mCursor.getString(3);
//                String expir=mCursor.getString(4);
//                String id=mCursor.getString(7);
//                FoodInfo fooditem=new FoodInfo();
//                fooditem.setName(name);
//                fooditem.setQuantity(quant);
//                fooditem.setMesure(measure);
//                fooditem.setExpirationDate(expir);
//                fooditem.setid(id);
//                FoodInfo.arrayList.add(fooditem);
//                mCursor.moveToNext();
//                i++;
//            }
//        }
//        return mCursor; // iterate to get each value.
//    }



    public int updateReminder(String name, String startDate, String color, String whichEye,
                              int noOfDays, int howOften, int noOfDrops,
                              int dropFrequency, String id){
        ContentValues values = new ContentValues();
        values.put(REMINDER_NAME, name);
        values.put(REMINDER_START_DATE, startDate);
        values.put(REMINDER_COLOR, color);
        values.put(REMINDER_WHICH_EYE, whichEye);
        values.put(REMINDER_NO_OF_DAYS, noOfDays);
        values.put(REMINDER_HOW_OFTEN, howOften);
        values.put(REMINDER_NO_OF_DROPS, noOfDrops);
        values.put(REMINDER_DROP_FREQUENCY, dropFrequency);
        return database.update(REMINDERS_TABLE, values, P_ID+"=?", new String[]{id});
    }

    public void deleteReminder(String id) {
        System.out.println("Comment deleted with id: " + id);
        database.delete(REMINDERS_TABLE, P_ID+"=?", new String[]{id});
    }

}
