package co.zamtech.mmt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import co.zamtech.mmt.Adapter.CommentAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.sFetchComment;
import co.zamtech.mmt.server.saddcomment;

public class AllComments extends AppCompatActivity {
    private ImageView aheader_back;
    private TextView aheader_title;
    private Context context;
    private Activity activity;
    private Loading loading;
    String secid,userid,secname;
    RecyclerView comments_recycler;
    private ArrayList<String> username=new ArrayList<>();
    private ArrayList<String> comment=new ArrayList<>();
    CommentAdapter adapter;
    EditText editcomment;
    ImageButton sendcomment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        init();
        fetchdata();
        aheader_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        sendcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedHelper.getKey(context, SharedHelper.islogin).equals("yes")) {
                    if(editcomment.getText().toString().trim().isEmpty() || editcomment.getText().toString().trim().equals(" ") || editcomment.getText().toString().trim().equals("")){
                        Toast.makeText(context,"Please type your comment.",Toast.LENGTH_SHORT).show();
                    }else{
                        @SuppressLint("StaticFieldLeak") final saddcomment sac=new saddcomment(){
                            @Override
                            protected void onPostExecute(String s) {
                                Log.i("checkzamar", s);
                                if(s.equals("error") || s.equals("Unable to connect to internet")){
                                    Log.d("check", "onPostExecute: No internet");

                                }else{
                                    try {
                                        JSONArray ja = new JSONArray(s);
                                        JSONObject jo;
                                        jo=ja.getJSONObject(0);
                                        if(!jo.getString("response").equals("yes")) {

                                        }
                                    }catch (JSONException e){
                                        e.printStackTrace();

                                    }
                                }
                                editcomment.setEnabled(true);
                                editcomment.setText("");
                            }
                        };
                        sac.execute(getString(R.string.baseURL)+"comments.php",secid,SharedHelper.getKey(context,SharedHelper.userid),editcomment.getText().toString(),"0");
                        editcomment.clearFocus();
                        editcomment.setEnabled(false);
                        username.add(SharedHelper.getKey(context,SharedHelper.name));
                        comment.add(editcomment.getText().toString());
                        adapter.notifyDataSetChanged();
                    }
                }else{
                    Toast.makeText(context,"Please Login to perform this function.",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    public void fetchdata(){
        @SuppressLint("StaticFieldLeak") final sFetchComment sfc=new sFetchComment(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    loading.hideLoading();
                }else{
                    try {
                        JSONArray ja_main = new JSONArray(s);
                        JSONObject jo_main;
                        username.clear();comment.clear();
                        for(int i=0;i<ja_main.length();i++){
                            jo_main=ja_main.getJSONObject(i);
                            username.add(jo_main.getString("username"));
                            comment.add(jo_main.getString("comment"));
                        }
                        comments_recycler.setLayoutManager(new LinearLayoutManager(context));
                        adapter= new CommentAdapter(username, comment,context);
                        comments_recycler.setAdapter(adapter);
                    }catch (JSONException e){
                        e.printStackTrace();
                        Snackbar.make(getWindow().getDecorView(), "Something went wrong.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    }
                    loading.hideLoading();
                }

            }
        };
        sfc.execute(getString(R.string.baseURL)+"allcomments.php",secid, SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
    }
    public void init(){
        secid= Objects.requireNonNull(getIntent().getExtras()).getString("secid");
        userid= Objects.requireNonNull(getIntent().getExtras()).getString("userid");
        secname= Objects.requireNonNull(getIntent().getExtras()).getString("secname");
        aheader_back=findViewById(R.id.aheader_back);
        aheader_title=findViewById(R.id.aheader_title);
        aheader_title.setText(secname);
        context=AllComments.this;
        activity=AllComments.this;
        loading=new Loading();
        loading.assignLoading(getWindow().getDecorView());
        comments_recycler=findViewById(R.id.comments_recycler);
        editcomment=findViewById(R.id.editcomment);
        sendcomment=findViewById(R.id.sendcomment);

    }
}
