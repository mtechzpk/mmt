package co.zamtech.mmt;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class EyeDropAlarm extends AppCompatActivity {

    private ImageView aheader_back;
    private TextView aheader_title;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabItem tabItem1, tabItem2, tabItem3;
    private PageAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eye_drop_alarm);

        aheader_back=findViewById(R.id.aheader_back);
        aheader_title=findViewById(R.id.aheader_title);
        aheader_title.setText("Eye Drop Alarm");
        aheader_back.setOnClickListener(v -> {
            finish();
        });

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabItem1 = (TabItem) findViewById(R.id.tab1);
        tabItem2 = (TabItem) findViewById(R.id.tab2);
        tabItem3 = (TabItem) findViewById(R.id.tab3);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);


        tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener<TabLayout.Tab>() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0){
                    pageAdapter.notifyDataSetChanged();
                } else if(tab.getPosition()==1){
                    pageAdapter.notifyDataSetChanged();
                } else if(tab.getPosition()==2) {
                    pageAdapter.notifyDataSetChanged();
                }
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }
}