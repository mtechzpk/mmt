package co.zamtech.mmt;

import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import co.zamtech.mmt.MySqlite.AlarmsModel;
import co.zamtech.mmt.MySqlite.RemindersModel;

public class Alarm {
    private int id;
    private String time;
    private int reminderId;
    private int requestCode;
    private boolean enableAlarm;
    private String alarmDate;
    private String alarmStatus;

    public Alarm(int id, String time, int requestCode, boolean enableAlarm,
                 String alarmDate, String alarmStatus, int reminderId) {
        this.id = id;
        this.time = time;
        this.reminderId = reminderId;
        this.requestCode = requestCode;
        this.enableAlarm = enableAlarm;
        this.alarmDate = alarmDate;
        this.alarmStatus = alarmStatus;
    }

    public Alarm() {

    }

    public void setAlarmStatus(String alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    public String getAlarmStatus() {
        return alarmStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getReminderId() {
        return reminderId;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setEnableAlarm(boolean enableAlarm) {
        this.enableAlarm = enableAlarm;
    }

    public boolean getEnableAlarm() {
        return enableAlarm;
    }

    public void setAlarmDate(String alarmDate) {
        this.alarmDate = alarmDate;
    }

    public String getAlarmDate() {
        return alarmDate;
    }

    public void setReminderId(int reminderId) {
        this.reminderId = reminderId;
    }

    public ArrayList<Alarm> getAlarmsByReminderId(Context context, int reminderId){

        AlarmsModel alarmsModel = new AlarmsModel(context);
        ArrayList<Alarm> alarmArrayList = new ArrayList<>();

        Cursor mCursor = alarmsModel.getAlarmsByReminderId(reminderId);
        Alarm alarm;

        boolean flag = false;
        while (!mCursor.isAfterLast()) {
            if(mCursor.getInt(3)==1) {
                flag = true;
            }
            alarm = new Alarm(mCursor.getInt(0),
                    mCursor.getString(1),
                    Integer.parseInt(mCursor.getString(2)),
                    flag,//enable alarm
                    mCursor.getString(4),
                    mCursor.getString(5),
                    mCursor.getInt(6));
            mCursor.moveToNext();

            alarmArrayList.add(alarm);
        }
        return alarmArrayList;
    }

    public ArrayList<Alarm> getAlarms(Context context){

        AlarmsModel alarmsModel = new AlarmsModel(context);
        ArrayList<Alarm> alarmArrayList = new ArrayList<>();

        Cursor mCursor = alarmsModel.selectAllAlarms();
        Alarm alarm;
        boolean flag = false;
        while (!mCursor.isAfterLast()) {
            if(mCursor.getInt(3)==1) {
                flag = true;
            }
            alarm = new Alarm(mCursor.getInt(0),
                    mCursor.getString(1),
                    Integer.parseInt(mCursor.getString(2)),
                    flag,//enable alarm
                    mCursor.getString(4),
                    mCursor.getString(5),
                    mCursor.getInt(6));
            mCursor.moveToNext();

            alarmArrayList.add(alarm);
        }
        return alarmArrayList;
    }

    public void deleteAlarm(Context context, int reminderId){
        AlarmsModel alarmsModel = new AlarmsModel(context);
        alarmsModel.deleteAlarm(String.valueOf(reminderId));
    }

    public boolean enableAlarm(Context context, int id){
        AlarmsModel alarmsModel = new AlarmsModel(context);
        int result = alarmsModel.enableAlarm(id);
        if(result > 0){
            return true;
        }
        return false;
    }

    public boolean disableAlarm(Context context, int id){
        AlarmsModel alarmsModel = new AlarmsModel(context);
        int result = alarmsModel.disableAlarm(id);
        if(result > 0){
            return true;
        }
        return false;
    }

    public String updateAlarm(String time, int reminderId, int requestCode, String date, String id,
                              Context context, String alarmStatus){
        AlarmsModel remindersModel = new AlarmsModel(context);
        int result = remindersModel.updateAlarm(time, reminderId, requestCode, date, id, alarmStatus);
        if(result > 0){
            return "Successfully updated ... ";
        }
        else{
            return "Something went wrong ...";
        }
    }

    public void printAlarm(){
        Log.e("Alarm Detail", "------------------------------");
        Log.e("id ", String.valueOf(this.getId()));
        Log.e("time ", this.getTime());
        Log.e("RequestCode ", String.valueOf(this.getRequestCode()));
        Log.e("enableAlarm ", String.valueOf(this.getEnableAlarm()));
        Log.e("alarm date ", this.getAlarmDate());
        Log.e("alarm status ", this.getAlarmStatus());
        Log.e("reminderid ", String.valueOf(this.getReminderId()));
    }
}
