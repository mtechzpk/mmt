package co.zamtech.mmt;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.util.Random;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class AlarmReciever extends BroadcastReceiver {

    private static final String CHANNEL_ID = "12";
    private int min = 20;
    private int max = 80;
    private int random = 0;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        //Toast.makeText(context ,"yes",Toast.LENGTH_SHORT).show();
        Reminder reminder = new Reminder();
        Log.e("lllll",String.valueOf(intent.getExtras().getInt("reminderId")));
        reminder = reminder.getRemindersById(context, intent.getExtras().getInt("reminderId"));
        long[] vibrate = { 0, 100, 200, 300 };
        NotificationManager mNotifyManager;
        NotificationCompat.Builder mBuilder;
        NotificationChannel notificationChannel;
        String NOTIFICATION_CHANNEL_ID = "17";

        try
        {
             intent=new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            mNotifyManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(context, null);
            mBuilder.setContentTitle(reminder.getName())
                    .setContentText(reminder.getWhichEye() +  " Eye")
                    .setTicker("Get details here..2")
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    //.setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {

                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                // Configure the notification channel.
                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                mNotifyManager.createNotificationChannel(notificationChannel);
            }

            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotifyManager.notify(random, mBuilder.build());
            //startForeground(17, mBuilder.build());


        }
        catch(Exception e)
        {
            Log.d("xx", "EXCEPTION IN SHOWING NOTIFICATION...\n");
            Log.e("xx", "Exception is : ", e);
        }


        /*int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Do something for lollipop and above versions
        } else if ( android.os.Build.VERSION_CODES.){
            // do something for phones running an SDK before lollipop
        }*/
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(reminder.getName())
                .setContentText(reminder.getWhichEye() +  " Eye")
                .setVibrate(vibrate)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                //.setDefaults(Notification.DEFAULT_SOUND);
        
        random = new Random().nextInt((max - min) + 1) + min;  //creating random ID for notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(random, builder.build());*/

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        tg.startTone(ToneGenerator.TONE_PROP_BEEP);*/
    }


}
