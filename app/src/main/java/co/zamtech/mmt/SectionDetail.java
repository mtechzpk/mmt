package co.zamtech.mmt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import co.zamtech.mmt.Adapter.slideshowAdapter;
import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;
import co.zamtech.mmt.server.sFetchSectionDetail;
import co.zamtech.mmt.server.sassignlike;

public class SectionDetail extends AppCompatActivity {
    private ImageView aheader_back;
    private TextView aheader_title;
    private Context context;
    private Activity activity;
    private Loading loading;
    String secid;
    TextView datetime;
    ViewPager sectionpager;
    TextView deslabel,des;
    ImageView likeimage,commentimage;
    TextView liketext,commenttext;
    ImageView playmainvideobcg,playmainvideo;
    ExtendedFloatingActionButton playvideo;
    String id,type,title,description,eventdate,eventtime,video,likestatus,totallikes,totalcomments;
    private ArrayList<String> slider=new ArrayList<>();
    private int currentPage = 0;
    private static Timer timer;
    private final long DELAY_MS = 1500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 5000; // time in milliseconds between successive task executions.
    private static Handler handler;
    private static Runnable update;
    private boolean isliked=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_detail);
        init();
        aheader_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        playmainvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview();
            }
        });
        playmainvideobcg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview();
            }
        });
        playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview();
            }
        });
        likeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likefunction();
            }
        });
        liketext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likefunction();
            }
        });
        commentimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentfunction();
            }
        });
        commenttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentfunction();
            }
        });
    }
    public void videoview(){
        Intent intent=new Intent(context,Player.class);
        //intent.putExtra("videourl","http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
        intent.putExtra("videourl",getResources().getString(R.string.baseURLImage)+video);
        startActivity(intent);
    }
    public void likefunction(){
        if (SharedHelper.getKey(context, SharedHelper.islogin).equals("yes")) {
            likeimage.setEnabled(false);
            liketext.setEnabled(false);
            if(isliked){
                isliked=false;
            }else{
                isliked=true;
            }
            assignlike();
            @SuppressLint("StaticFieldLeak") final sassignlike sal=new sassignlike(){
                @Override
                protected void onPostExecute(String s) {
                    Log.i("checkzamar", s);
                    if(s.equals("error") || s.equals("Unable to connect to internet")){
                        Log.d("check", "onPostExecute: No internet");
                        if(isliked){
                            isliked=false;
                        }else{
                            isliked=true;
                        }
                        assignlike();
                    }else{
                        try {
                            JSONArray ja = new JSONArray(s);
                            JSONObject jo;
                            jo=ja.getJSONObject(0);
                            if(!jo.getString("response").equals("yes")) {
                                if (isliked) {
                                    isliked = false;
                                } else {
                                    isliked = true;
                                }
                                assignlike();
                            }else{
                                if(isliked){
                                    int templike=Integer.parseInt(totallikes);
                                    templike++;
                                    totallikes=String.valueOf(templike);
                                    liketext.setText(templike+" Likes");
                                }else{
                                    int templike=Integer.parseInt(totallikes);
                                    templike--;
                                    totallikes=String.valueOf(templike);
                                    liketext.setText(templike+" Likes");
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                            if(isliked){
                                isliked=false;
                            }else{
                                isliked=true;
                            }
                            assignlike();
                        }
                    }
                    likeimage.setEnabled(true);
                    liketext.setEnabled(true);

                }
            };
            if(isliked){
                sal.execute(getString(R.string.baseURL)+"likes.php",secid,SharedHelper.getKey(context,SharedHelper.userid),"0");
            }else{
                sal.execute(getString(R.string.baseURL)+"likes.php",secid,SharedHelper.getKey(context,SharedHelper.userid),"1");
            }
        }else{
            Toast.makeText(context,"Please Login to perform this function.",Toast.LENGTH_SHORT).show();
        }


    }
    public void commentfunction(){
        Intent intent=new Intent(context,AllComments.class);
        intent.putExtra("userid",SharedHelper.getKey(context,SharedHelper.userid));
        intent.putExtra("secid",secid);
        intent.putExtra("secname",title);
        startActivity(intent);
    }
    public void assignlike(){
        if(isliked){
            likeimage.setImageResource(R.drawable.likesel);
            liketext.setTextColor(getResources().getColor(R.color.colorBlue));

        }else{
            likeimage.setImageResource(R.drawable.like);
            liketext.setTextColor(getResources().getColor(R.color.colorBlack));

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(handler!=null) {
            timer.cancel();
            handler.removeCallbacksAndMessages(update);
        }
        fetchdata();
    }
    @Override
    public void onPause() {
        if(handler!=null){
            timer.cancel();
            handler.removeCallbacksAndMessages(update);
        }
        super.onPause();
    }
    public void fetchdata(){
        @SuppressLint("StaticFieldLeak") final sFetchSectionDetail sfsd=new sFetchSectionDetail(){
            @Override
            protected void onPostExecute(String s) {
                Log.i("checkzamar", s);
                if(s.equals("error") || s.equals("Unable to connect to internet")){
                    Log.d("check", "onPostExecute: No internet");
                    Snackbar.make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    loading.hideLoading();
                }else{
                    try {
                        JSONObject jsonObj = new JSONObject(s);

//extracting data array from json string
                        JSONArray ja_main = jsonObj.getJSONArray("section");
                        JSONObject json_main = ja_main.getJSONObject(0);
                        if(json_main.getString("response").equals("yes")){
                            id=json_main.getString("id");
                            type=json_main.getString("type");
                            title=json_main.getString("title");
                            description=json_main.getString("description");
                            eventdate=json_main.getString("eventdate");
                            eventtime=json_main.getString("eventtime");
                            video=json_main.getString("video");
                            likestatus=json_main.getString("likestatus");
                            totallikes=json_main.getString("totallikes");
                            totalcomments=json_main.getString("totalcomments");
                            if(type.equals("2")){
                                datetime.setText(eventdate+" "+eventtime);
                            }else{
                                datetime.setVisibility(View.GONE);
                            }
                            if(SharedHelper.getKey(context,SharedHelper.lang).equals("1")){
                                    try {
                                        aheader_title.setText(new String(title.getBytes("ISO-8859-1"), "utf-8"));
                                        des.setText(new String(description.getBytes("ISO-8859-1"), "utf-8"));
                                    }catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                            }else{
                                aheader_title.setText(title);
                                des.setText(description);
                            }

                            if(likestatus.equals("1")){
                                isliked=true;
                            }else{
                                isliked=false;
                            }
                            liketext.setText(totallikes+" Likes");
                            commenttext.setText(totalcomments+" Comments");
                            assignlike();
                        }else{
                            Snackbar.make(getWindow().getDecorView(), "Something went wrong.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                        }
                        JSONArray ja_image = jsonObj.getJSONArray("images");
                        JSONObject json_image = ja_image.getJSONObject(0);
                        if(json_image.getString("response").equals("yes")){
                            slider.clear();
                            for(int i=0;i<ja_image.length();i++){
                                json_image=ja_image.getJSONObject(i);
                                slider.add(json_image.getString("image"));
                            }
                            slideshowAdapter vAdp = new slideshowAdapter(context,slider);
                            sectionpager.setAdapter(vAdp);
                            handler = new Handler();
                            update = new Runnable() {
                                public void run() {
                                    if (currentPage == slider.size()) {
                                        currentPage = 0;
                                    }
                                    sectionpager.setCurrentItem(currentPage++, true);
                                }
                            };
                            timer = new Timer(); // This will create a new Thread
                            timer.schedule(new TimerTask() { // task to be scheduled

                                @Override
                                public void run() {
                                    handler.post(update);
                                }
                            }, DELAY_MS, PERIOD_MS);
                            if(video.equals("")){
                                playmainvideo.setVisibility(View.GONE);
                                playmainvideobcg.setVisibility(View.GONE);
                                playvideo.setVisibility(View.GONE);
                            }else{
                                playmainvideo.setVisibility(View.GONE);
                                playmainvideobcg.setVisibility(View.GONE);
                                playvideo.setVisibility(View.VISIBLE);
                            }
                        }else{
                            playvideo.setVisibility(View.GONE);
                            if(video.equals("")){
                                sectionpager.setVisibility(View.GONE);
                                playmainvideo.setVisibility(View.GONE);
                                playmainvideobcg.setVisibility(View.GONE);
                            }else{
                                sectionpager.setVisibility(View.INVISIBLE);
                            }
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                        Snackbar.make(getWindow().getDecorView(), "Something went wrong.", Snackbar.LENGTH_SHORT).setAction("Action", null).setBackgroundTint(ContextCompat.getColor(context,R.color.snackbarcolor)).show();
                    }
                    loading.hideLoading();
                }

            }
        };
        sfsd.execute(getString(R.string.baseURL)+"sections.php",secid,SharedHelper.getKey(context,SharedHelper.userid), SharedHelper.getKey(context,SharedHelper.lang));
        loading.showLoading();
    }
    public void init(){
        secid= Objects.requireNonNull(getIntent().getExtras()).getString("secid");
        aheader_back=findViewById(R.id.aheader_back);
        aheader_title=findViewById(R.id.aheader_title);
        context=SectionDetail.this;
        activity=SectionDetail.this;
        loading=new Loading();
        loading.assignLoading(getWindow().getDecorView());
        datetime=findViewById(R.id.datetime);
        sectionpager=findViewById(R.id.sectionpager);
        deslabel=findViewById(R.id.deslabel);
        des=findViewById(R.id.des);
        likeimage=findViewById(R.id.likeimage);
        commentimage=findViewById(R.id.commentimage);
        liketext=findViewById(R.id.liketext);
        commenttext=findViewById(R.id.commenttext);
        playmainvideobcg=findViewById(R.id.playmainvideobcg);
        playmainvideo=findViewById(R.id.playmainvideo);
        playvideo=findViewById(R.id.playvideo);
        if(SharedHelper.getKey(context,SharedHelper.lang).equals("1"))
            deslabel.setText("تفصیل");

    }
}
