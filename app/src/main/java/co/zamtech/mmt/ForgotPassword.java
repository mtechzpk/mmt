package co.zamtech.mmt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import co.zamtech.mmt.Helper.Loading;
import co.zamtech.mmt.Helper.SharedHelper;

public class ForgotPassword extends AppCompatActivity {
    EditText forgotpassword_name;
    ExtendedFloatingActionButton forgotpassword_button;
    TextView forgotpassword_label;
    Loading loading;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
        loading=new Loading();
        loading.assignLoading(findViewById(android.R.id.content).getRootView());
        forgotpassword_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
    public void init(){
        context=this;
        forgotpassword_name=findViewById(R.id.forgotpassword_name);
        forgotpassword_label=findViewById(R.id.forgotpassword_label);
        forgotpassword_button=findViewById(R.id.forgotpassword_button);
        if (SharedHelper.getKey(context, SharedHelper.lang).equals("1")) {
            forgotpassword_name.setHint("اپنا ای میل پتہ درج کریں ");
            forgotpassword_label.setText("پاسورڈ بھول گے ");
            forgotpassword_button.setText("بھیجیں ");
        }
    }
}
