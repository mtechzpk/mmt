package com.example.alarmmanager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Map;
import java.util.Random;

public class BackgroundBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sendOreoNotification(context);
        }
        sendNotification(context);
    }

    public void sendOreoNotification(Context context){

            //RemoteMessage.Notification notification = remoteMessage.getNotification();


            OreoNotification oreoNotification = new OreoNotification(context);

            Notification.Builder builder = oreoNotification.getOreoNotification("Alarm","Received Alarm", String.valueOf(R.drawable.profile_pic_icon));
            builder.setAutoCancel(true);

            oreoNotification.getManager().notify(new Random().nextInt(),builder.build());

        }




    public void sendNotification(Context context) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notify")
                .setSmallIcon(R.drawable.profile_pic_icon)
                .setContentTitle("Alarm")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText("Received Alarm");
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(200, builder.build());

    }
}


